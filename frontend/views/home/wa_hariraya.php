<div class="birthday">
    <div class="row no-gutters">
        <div class="col-md-1">
            <img src="<?php echo \Yii::getAlias('@web').'/asset/images/reload.png' ?>" alt="">
        </div>
        <div class="col-md-11">
            <div class="kirim-pesan">
                <a href="#">
                    <p>KIRIM PESAN WA</p>
                </a>
            </div>
            <div class="card">
                <div class="card-header">
                    <div class="row atas-sendiri">
                        <div class="col-md-8">
                            <div class="template">
                                <p><span><img src="<?php echo \Yii::getAlias('@web').'/asset/images/birth.png' ?>" alt=""></span>TEMPLATE BULAN RAMADHAN</p>
                            </div>
                        </div>
                        
                        <div class="col-md-4">
                        <div class="form-check form-check-inline">
                            <label class="form-check-label status" for="inlineRadio1">STATUS</label>
                            <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1">
                            <label class="form-check-label" for="inlineRadio1">AKTIF</label>
                            </div>
                            <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2">
                            <label class="form-check-label" for="inlineRadio2">NON AKTIF</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="content">
                        <p>Dear [NAMA], Happy Birthday! Kami dari keluarga Mobil Karawang mendoakan anda kesehatan, kesuksesan dan kebahagiaan! Semakin baik, semakin bijak dan selalu ingat Mobil Karawang jika mencari mobil atau ingin menjual mobilnya. </p>
                    </div>
                </div>
            </div>
            <div class="row pt-3 no-gutters">
                <div class="col-md-8">
                    <div class="bawah-card">
                        <p>Kode [NAMA] akan otomatis tergantikan nama pelanggan ketika pesan dikirim.</p>
                    </div>
                </div>
                <div class="col-md-4 text-right">
                    <button class="btn btn-info">UPDATE TEMPLATE</button>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="py-4"></div>
<div class="birthday">
    <div class="row no-gutters">
        <div class="col-md-1">
            <!-- <img src="<?php echo \Yii::getAlias('@web').'/asset/images/reload.png' ?>" alt=""> -->
        </div>
        <div class="col-md-11">
            <!-- <div class="kirim-pesan">
                <a href="#">
                    <p>KIRIM PESAN WA</p>
                </a>
            </div> -->
            <div class="card">
                <div class="card-header">
                    <div class="row atas-sendiri">
                        <div class="col-md-8">
                            <div class="template">
                                <p><span><img src="<?php echo \Yii::getAlias('@web').'/asset/images/birth.png' ?>" alt=""></span>TEMPLATE IDUL FITRI</p>
                            </div>
                        </div>                        <div class="col-md-4">
                        <div class="form-check form-check-inline">
                            <label class="form-check-label status" for="inlineRadio1">STATUS</label>
                            <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1">
                            <label class="form-check-label" for="inlineRadio1">AKTIF</label>
                            </div>
                            <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2">
                            <label class="form-check-label" for="inlineRadio2">NON AKTIF</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="content">
                        <p>Dear [NAMA], Happy Birthday! Kami dari keluarga Mobil Karawang mendoakan anda kesehatan, kesuksesan dan kebahagiaan! Semakin baik, semakin bijak dan selalu ingat Mobil Karawang jika mencari mobil atau ingin menjual mobilnya. </p>
                    </div>
                </div>
            </div>
            <div class="row pt-3 no-gutters">
                <div class="col-md-8">
                    <div class="bawah-card">
                        <p>Kode [NAMA] akan otomatis tergantikan nama pelanggan ketika pesan dikirim.</p>
                    </div>
                </div>
                <div class="col-md-4 text-right">
                    <button class="btn btn-info">UPDATE TEMPLATE</button>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="py-4"></div>
<div class="birthday">
    <div class="row no-gutters">
        <div class="col-md-1">
            <!-- <img src="<?php echo \Yii::getAlias('@web').'/asset/images/reload.png' ?>" alt=""> -->
        </div>
        <div class="col-md-11">
            <!-- <div class="kirim-pesan">
                <a href="#">
                    <p>KIRIM PESAN WA</p>
                </a>
            </div> -->
            <div class="card">
                <div class="card-header">
                    <div class="row atas-sendiri">
                        <div class="col-md-8">
                            <div class="template">
                                <p><span><img src="<?php echo \Yii::getAlias('@web').'/asset/images/birth.png' ?>" alt=""></span>TEMPLATE NATAL</p>
                            </div>
                        </div>
                        <div class="col-md-4">
                        <div class="form-check form-check-inline">
                            <label class="form-check-label status" for="inlineRadio1">STATUS</label>
                            <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1">
                            <label class="form-check-label" for="inlineRadio1">AKTIF</label>
                            </div>
                            <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2">
                            <label class="form-check-label" for="inlineRadio2">NON AKTIF</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="content">
                        <p>Dear [NAMA], Happy Birthday! Kami dari keluarga Mobil Karawang mendoakan anda kesehatan, kesuksesan dan kebahagiaan! Semakin baik, semakin bijak dan selalu ingat Mobil Karawang jika mencari mobil atau ingin menjual mobilnya. </p>
                    </div>
                </div>
            </div>
            <div class="row pt-3 no-gutters">
                <div class="col-md-8">
                    <div class="bawah-card">
                        <p>Kode [NAMA] akan otomatis tergantikan nama pelanggan ketika pesan dikirim.</p>
                    </div>
                </div>
                <div class="col-md-4 text-right">
                    <button class="btn btn-info">UPDATE TEMPLATE</button>
                </div>
            </div>
        </div>
    </div>
</div>

