<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
?>
<div class="insides-pg inners_page_editing">
	<div class="top-inner">
		
	</div>

	<div class="content inners_pglist_car insert_data">
		<div class="inners-top-status-mobil">
			<div class="inns_top_leftTops pt-3 pb-4">
				<span>DATA KENDARAAN & DATA PENJUAL</span>
			</div>
			<div class="row btm_pembelians pt-3 pb-3">
				<div class="col-md-6">
					<p><span>DATA PEMBELIAN</span> <br>
					<?php if ($model->status_terjual): ?>
					mobil laku terjual
					<?php else: ?>
					belum ada data pembelian / mobil belum laku terjual
					<?php endif ?>
					</p>
				</div>
				<div class="col-md-6 text-right d-block">
					<?php if ( isset($modelPembelian) AND $modelPembelian->nama_pembeli != '' ): ?>
					<a href="<?php echo Url::toRoute(['home/newvehicle_edit', 'id'=> $model->id]) ?>" class="btn btn-warning btns_submitn_frmyellow">LIHAT DATA</a>
					<?php else: ?>
					<a href="<?php echo Url::toRoute(['home/newvehicle_edit', 'id'=> $model->id]) ?>" class="btn btn-warning btns_submitn_frmyellow">UPDATE STATUS</a>
					<?php endif; ?>
				</div>
			</div>
			<div class="clear clearfix"></div>
		</div>

		<div class="py-1"></div>
		<div class="lines-white d-block my-4"></div>
		<div class="py-1"></div>
		<div class="box-form-widget pt-3">
		<div class="n innerstop mb-4">
			<span>DATA KENDARAAN & DATA PENJUAL</span>
		</div>
		<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
			<div class="row">
				<div class="col-md-6">
			 		<?= $form->field($model, 'tgl_beli', [
				                    'template' => "<div class='form-group row'>{label}\n<div class='col-md-5'>{input}</div>\n{hint}\n{error}</div>",
				                    'labelOptions' => [ 'class' => 'col-sm-4 col-form-label' ],
				                    'inputOptions' => [ 'class' => 'disable_inputs', 'readonly'=>'readonly' ]
				    ])->textInput(['maxlength' => true])?>
					<?= $form->field($model, 'no_polisi', [
				                    'template' => "<div class='form-group row'>{label}\n<div class='col-md-8'>{input}</div>\n{hint}\n{error}</div>",
				                    'labelOptions' => [ 'class' => 'col-sm-4 col-form-label' ],
				                    'inputOptions' => [ 'class' => 'disable_inputs', 'readonly'=>'readonly' ]
				    ])->textInput(['maxlength' => true])?>

				    <?= $form->field($model, 'atas_nama_stnk', [
				                    'template' => "<div class='form-group row'>{label}\n<div class='col-md-8'>{input}</div>\n{hint}\n{error}</div>",
				                    'labelOptions' => [ 'class' => 'col-sm-4 col-form-label' ],
				                    'inputOptions' => [ 'class' => 'disable_inputs', 'readonly'=>'readonly' ]
				    ])->textInput(['maxlength' => true])?>
				    <?= $form->field($model, 'alamat_stnk', [
				                    'template' => "<div class='form-group row'>{label}\n<div class='col-md-8'>{input}</div>\n{hint}\n{error}</div>",
				                    'labelOptions' => [ 'class' => 'col-sm-4 col-form-label' ],
				                    'inputOptions' => [ 'class' => 'disable_inputs', 'readonly'=>'readonly' ]
				    ])->textInput(['maxlength' => true])?>
				    <?= $form->field($model, 'no_rangka', [
				                    'template' => "<div class='form-group row'>{label}\n<div class='col-md-8'>{input}</div>\n{hint}\n{error}</div>",
				                    'labelOptions' => [ 'class' => 'col-sm-4 col-form-label' ],
				                    'inputOptions' => [ 'class' => 'disable_inputs', 'readonly'=>'readonly' ]
				    ])->textInput(['maxlength' => true])?>
				    <?= $form->field($model, 'no_mesin', [
				                    'template' => "<div class='form-group row'>{label}\n<div class='col-md-8'>{input}</div>\n{hint}\n{error}</div>",
				                    'labelOptions' => [ 'class' => 'col-sm-4 col-form-label' ],
				                    'inputOptions' => [ 'class' => 'disable_inputs', 'readonly'=>'readonly' ]
				    ])->textInput(['maxlength' => true])?>
				    <?= $form->field($model, 'jenis_kendaraan', [
				                    'template' => "<div class='form-group row'>{label}\n<div class='col-md-8'>{input}</div>\n{hint}\n{error}</div>",
				                    'labelOptions' => [ 'class' => 'col-sm-4 col-form-label' ],
				                    'inputOptions' => [ 'class' => 'disable_inputs', 'readonly'=>'readonly' ]
				    ])->textInput(['maxlength' => true])?>
				    <?= $form->field($model, 'warna', [
				                    'template' => "<div class='form-group row'>{label}\n<div class='col-md-8'>{input}</div>\n{hint}\n{error}</div>",
				                    'labelOptions' => [ 'class' => 'col-sm-4 col-form-label' ],
				                    'inputOptions' => [ 'class' => 'disable_inputs', 'readonly'=>'readonly' ]
				    ])->textInput(['maxlength' => true])?>
				    <?= $form->field($model, 'tahun_kendaraan', [
				                    'template' => "<div class='form-group row'>{label}\n<div class='col-md-8'>{input}</div>\n{hint}\n{error}</div>",
				                    'labelOptions' => [ 'class' => 'col-sm-4 col-form-label' ],
				                    'inputOptions' => [ 'class' => 'disable_inputs', 'readonly'=>'readonly' ]
				    ])->textInput(['maxlength' => true])?>
				    <?= $form->field($model, 'tanggal_berlaku_pajak', [
				                    'template' => "<div class='form-group row'>{label}\n<div class='col-md-8'>{input}</div>\n{hint}\n{error}</div>",
				                    'labelOptions' => [ 'class' => 'col-sm-4 col-form-label' ],
				                    'inputOptions' => [ 'class' => 'disable_inputs', 'readonly'=>'readonly' ]
				    ])->textInput(['maxlength' => true])?>
				    

				</div>
				<div class="col-md-6">
					<?= $form->field($model, 'nama_penjual', [
				                    'template' => "<div class='form-group row'>{label}\n<div class='col-md-8'>{input}</div>\n{hint}\n{error}</div>",
				                    'labelOptions' => [ 'class' => 'col-sm-4 col-form-label' ],
				                    'inputOptions' => [ 'class' => 'disable_inputs', 'readonly'=>'readonly' ]
				    ])->textInput(['maxlength' => true])?>
				    <?= $form->field($model, 'hp_penjual', [
				                    'template' => "<div class='form-group row'>{label}\n<div class='col-md-8'>{input}</div>\n{hint}\n{error}</div>",
				                    'labelOptions' => [ 'class' => 'col-sm-4 col-form-label' ],
				                    'inputOptions' => [ 'class' => 'disable_inputs', 'readonly'=>'readonly' ]
				    ])->textInput(['maxlength' => true])?>
				    <?= $form->field($model, 'alamat_penjual', [
				                    'template' => "<div class='form-group row'>{label}\n<div class='col-md-8'>{input}</div>\n{hint}\n{error}</div>",
				                    'labelOptions' => [ 'class' => 'col-sm-4 col-form-label' ],
				                    'inputOptions' => [ 'class' => 'disable_inputs', 'readonly'=>'readonly' ]
				    ])->textInput(['maxlength' => true])?>
				    <?= $form->field($model, 'tanggal_lahir_penjual', [
				                    'template' => "<div class='form-group row'>{label}\n<div class='col-md-8'>{input}</div>\n{hint}\n{error}</div>",
				                    'labelOptions' => [ 'class' => 'col-sm-4 col-form-label' ],
				                    'inputOptions' => [ 'class' => 'disable_inputs', 'readonly'=>'readonly' ]
				    ])->textInput(['maxlength' => true])?>
				    <?= $form->field($model, 'total_perbaikan', [
				                    'template' => "<div class='form-group row'>{label}\n<div class='col-md-8'>{input}</div>\n{hint}\n{error}</div>",
				                    'labelOptions' => [ 'class' => 'col-sm-4 col-form-label' ],
				                    'inputOptions' => [ 'class' => 'disable_inputs', 'readonly'=>'readonly' ]
				    ])->textInput(['maxlength' => true])?>
				    <?= $form->field($model, 'harga_beli', [
				                    'template' => "<div class='form-group row'>{label}\n<div class='col-md-8'>{input}</div>\n{hint}\n{error}</div>",
				                    'labelOptions' => [ 'class' => 'col-sm-4 col-form-label' ],
				                    'inputOptions' => [ 'class' => 'disable_inputs', 'readonly'=>'readonly' ]
				    ])->textInput(['maxlength' => true])?>

				    <div class='form-group row d-none d-sm-block'>
						<label class="col-sm-8 col-form-label">&nbsp;</label>
						<div class='col-md-4'>&nbsp;</div>
					</div>

					<div class='form-group row'>
						<label class="col-sm-4 col-form-label">GALLERY DATA DOKUMEN</label>
						<div class="col-md-8 mx-auto text-left">
							<?php if (count($modelDocum) <= 0): ?>
					        <!-- empty -->
					    	<?php else: ?>
					    	<ul id="light-gallery" class="gallery light-gallery list-inline gallerys_lisnline">
					    	<?php foreach ($modelDocum as $key => $value): ?>
				        		<li data-src="<?php echo Yii::getAlias('@web').'/uploads/dokumen/'.$value->images ?>">
							        	<a href="<?php echo Yii::getAlias('@web').'/uploads/dokumen/'.$value->images ?>" class="btn btn-secondary butns_ingrey">
							            <img src="<?php echo Yii::getAlias('@web').'/uploads/dokumen/'.$value->images ?>" alt="" class="img img-fluid img-thumbnail">
							            </a>
							        </li>
					    	<?php endforeach ?>
					    	</ul>
					    	<?php endif ?>
						</div>
					</div>

					<?php
					/*
					$lsn_ar_right = [
									'dok_stnk',
									'dok_bpkb',
									'dok_faktur',
									'dok_copyktp_stnk',
									'dok_surat_buka_blokir',
									'dok_pelepasan_hak',
									'dok_kwitansi_blangko',
									'dok_ktp_penjual',
									'dok_foto_penjual',
									'dok_nota_bon1',
									'dok_nota_bon2',
									'dok_nota_bon3',
									'dok_nota_bon4',
									'dok_nota_bon5',
									];
					?>
					<?php foreach ($lsn_ar_right as $key => $value): ?>
					<div class="form-group row">
					    <label class="col-sm-4 col-form-label" for="mastermobil-dok_stnk"><?= Html::activeLabel($model, $value); ?></label>
					    <div class="col-md-8 mx-auto text-center">
					    	<?php if ($model->{$value} == '' OR $model->{$value} == null): ?>
					        -
					    	<?php else: ?>
					        <a href="<?php echo Yii::getAlias('@web').'/uploads/mobil/'.$model->{$value} ?>" data-fancybox="images" class="btn btn-secondary butns_ingrey">LIHAT</a>
					    	<?php endif ?>
					    </div>
					</div>
					<?php endforeach; */ ?>

					<div class='form-group row d-none d-sm-block'>
						<label class="col-sm-8 col-form-label">&nbsp;</label>
						<div class='col-md-4'>&nbsp;</div>
					</div>

					<div class="form-group row">
					    <label class="col-sm-4 col-form-label" for="mastermobil-dok_stnk">GALLERY PHOTO KENDARAAN</label>
					    <div class="col-md-8 mx-auto text-left">
					    	<?php if (count($modelGallery) <= 0): ?>
					        <!-- empty -->
					    	<?php else: ?>
					    	<ul id="light-gallery" class="gallery light-gallery list-inline gallerys_lisnline">
					    	<?php foreach ($modelGallery as $key => $value): ?>
				        		<li data-src="<?php echo Yii::getAlias('@web').'/uploads/gallery/'.$value->images ?>">
							        	<a href="<?php echo Yii::getAlias('@web').'/uploads/gallery/'.$value->images ?>" class="btn btn-secondary butns_ingrey">
							            <img src="<?php echo Yii::getAlias('@web').'/uploads/gallery/'.$value->images ?>" alt="" class="img img-fluid img-thumbnail">
							            </a>
							        </li>
					    	<?php endforeach ?>
					    	</ul>
					    	<?php endif ?>
					    </div>
					</div>

					<div class="clearfix clear"></div>
				</div>
			</div>

			<div class="py-2"></div>
			<div class="row">
				<div class="col-md-6"></div>
				<div class="col-md-6">
				    <div class="form-group">
				        <?php // echo Html::button('UPDATE DATA', ['class' => 'btn btn-primary btns_submitn_frmyellow', 'onclick'=> 'javascript:;']) ?>
				        <a href="<?php echo Url::toRoute(['home/newvehicle_edit_master', 'id'=> $model->id]) ?>" class="btn btn-warning btns_submitn_frmyellow">UPDATE DATA</a>
				    </div>
				</div>
			</div>
			<?php ActiveForm::end(); ?>
		</div>
		<div class="clear"></div>
	</div>
	<div class="clearfix"></div>
</div>

<script type="text/javascript">
	$(document).ready(function() {
		$(".light-gallery").lightGallery();
	});
</script>