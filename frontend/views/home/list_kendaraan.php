<?php 
use yii\helpers\Url;
?>
<div class="insides-pg">
	<div class="top-inner">
		
	</div>

	<div class="content inners_pglist_car">
		<div class="inns_top_leftTops">
			<span>HASIL PENCARIAN</span>
		</div>
		<?php if (Yii::$app->session->hasFlash('success')): ?>
		    <div class="alert alert-success" role="alert">
		        <?= Yii::$app->session->getFlash('success') ?>
		    </div>
		<?php endif; ?>
		<?php if (Yii::$app->session->hasFlash('error')): ?>
		    <div class="alert alert-danger" role="alert">
		        <?= Yii::$app->session->getFlash('error') ?>
		    </div>
		<?php endif; ?>
		<div class="table-responsive">
			<table class="customs_table table list_table">
				<thead class="thead-dark">
					<tr>
						<th>NO POLISI</th>
						<th>JENIS KENDARAAN</th>
						<th>TAHUN KENDARAAN</th>
						<th>TANGGAL BELI</th>
						<th>NAMA PENJUAL</th>
						<th>HP PENJUAL</th>
						<th>NAMA PEMBELI</th>
						<th>HP PEMBELI</th>
						<th>STATUS</th>
						<th>-</th>
					</tr>
				</thead>
			</table>
		</div>
		<div class="clear"></div>
	</div>
	<div class="clearfix"></div>
</div>

<script type="text/javascript">
	$(function(){

		var responseObj = [
			<?php if ($total > 0): ?>
				<?php $i = 1; ?>
				<?php foreach ($model as $key => $value): ?>
			    { "no_polisi": "<?php echo $value->no_polisi ?>", "jenis_kendaraan": "<?php echo $value->jenis_kendaraan ?>", "tahun_kendaraan": "<?php echo $value->tahun_kendaraan ?>", "tanggal_beli": "<?php echo $value->tgl_beli ?>", "nama_penjual": "<?php echo $value->nama_penjual ?>", "hp_penjual": "<?php echo $value->hp_penjual ?>", "nama_pembeli": "-", "hp_pembeli": "-", "status_terjual": "<?php echo ($value->status_terjual == 1)? 'TERJUAL':'BELUM TERJUAL' ?>", "links":"<?php echo Url::toRoute(['home/newvehicle_view', 'id' => $value->id]) ?>" }<?php if ($i != $total): ?>,<?php endif ?>
			    <?php $i = $key + 1; ?>
				<?php endforeach ?>
			<?php endif ?>
		];

		$('.customs_table').DataTable( {
					"columnDefs": 
						[
						    { "orderable": false, "targets": [0,8,9,5,6,7] }
						],
						"data": responseObj,
						"columns": [
						  { "data": "no_polisi" },
						  { "data": "jenis_kendaraan" },
						  { "data": "tahun_kendaraan" },
						  { "data": "tanggal_beli" },
						  { "data": "nama_penjual" },
						  { "data": "hp_penjual" },
						  { "data": "nama_pembeli" },
						  { "data": "hp_pembeli" },
						  { "data": "status_terjual" },
						  {
						     "data": "links",
						     "render": function(data, type, row, meta){
						        if(type === 'display'){
						            data = '<a href="' + data + '"><i class="fa fa-pencil"></a>';
						        }
						        return data;
						     }
						  } 
						]
						<?php if (isset($_GET['search'])): ?>
						,
						"oSearch": {"sSearch": "<?php echo htmlentities(htmlspecialchars($_GET['search'])) ?>" }
						<?php endif ?>
			  	});

	    var sm_txlist = 'Klik pada hasil pencarian yang sesuai untuk melihat atau memperbarui data';
	 	$('.inners_pglist_car .dataTables_wrapper .dataTables_info').html(sm_txlist);
	 	
	});
</script>