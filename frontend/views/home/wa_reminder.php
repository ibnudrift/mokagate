<?php
use yii\widgets\ActiveForm;
?>
<div class="birthday">
    <div class="row no-gutters">
        <div class="col-md-1">
            <img src="<?php echo \Yii::getAlias('@web').'/asset/images/reload.png' ?>" alt="">
        </div>
        <div class="col-md-11">
            <div class="kirim-pesan">
                <a href="#">
                    <p>KIRIM PESAN WA</p>
                </a>
            </div>
            <?php if (Yii::$app->session->hasFlash('success')): ?>
                <div class="alert alert-success alert-dismissable">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <h4><i class="icon fa fa-check"></i>Saved!</h4>
                    <?= Yii::$app->session->getFlash('success') ?>
                </div>
            <?php endif; ?>
            <?php if (Yii::$app->session->hasFlash('error')): ?>
                <div class="alert alert-danger alert-dismissable">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <h4><i class="icon fa fa-check"></i>Failed!</h4>
                    <?= Yii::$app->session->getFlash('error') ?>
                </div>
            <?php endif; ?>
            <?php
            if($model->hasErrors()){
                echo Html::errorSummary($model, [
                    'class'=>'alert alert-danger alert-dismissable',
                ]);
            }
            ?>
            <?php foreach ($result->data as $key => $value) { ?>
<?php
if (isset($value->pesan)) {
    $model->pesan = $value->pesan;
    if ($value->pesan != '') {
        $model->is_active = 1;
    } else {
        $model->is_active = 0;
    }
}
?>
            <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
            <input type="hidden" name="ReminderForm[id]" value="<?php echo $value->id ?>">
            <div class="card">
                <div class="card-header">
                    <div class="row atas-sendiri">
                        <div class="col-md-8">
                            <div class="template">
                                <p><span><img src="<?php echo \Yii::getAlias('@web').'/asset/images/icons-plancs.png' ?>" alt=""></span>TEMPLATE UCAPAN REMINDER KEPADA PENJUAL</p>
                                <div class="pl-5 ml-3">
                                    <label for="" class="d-inline align-middle">KIRIM KETIKA</label> &nbsp;&nbsp;&nbsp;
                                    <input type="text" name="ReminderForm[hari_ke]" value="<?php echo $value->hari_ke ?>" class="form-control d-inline-block align-middle mr-3" style="max-width: 12%; padding: 0.3rem;">
                                    <!-- <select name="ReminderForm[hari_ke]" id="select-hari-ke-<?php echo $value->id ?>" class="form-control d-inline-block align-middle mr-3" style="max-width: 12%; padding: 0.3rem;">
                                        <option value="1">1</option>
                                        <option value="7">7</option>
                                        <option value="14">14</option>
                                        <option value="21">21</option>
                                        <option value="30">30</option>
                                        <option value="60">60</option>
                                        <option value="90">90</option>
                                        <option value="120">120</option>
                                        <option value="150">150</option>
                                        <option value="180">180</option>
                                        <option value="210">210</option>
                                        <option value="240">240</option>
                                        <option value="270">270</option>
                                        <option value="300">300</option>
                                        <option value="330">330</option>
                                        <option value="360">360</option>
                                        <option value="390">390</option>
                                        <option value="420">420</option>
                                        <option value="450">450</option>
                                        <option value="480">480</option>
                                        <option value="510">510</option>
                                        <option value="540">540</option>
                                        <option value="570">570</option>
                                        <option value="600">600</option>
                                        <option value="630">630</option>
                                        <option value="660">660</option>
                                        <option value="690">690</option>
                                        <option value="720">720</option>
                                        <option value="750">750</option>
                                        <option value="780">780</option>
                                        <option value="810">810</option>
                                        <option value="840">840</option>
                                        <option value="870">870</option>
                                        <option value="900">900</option>
                                        <option value="930">930</option>
                                        <option value="960">960</option>
                                        <option value="990">990</option>
                                        <option value="1020">1020</option>
                                        <option value="1050">1050</option>
                                        <option value="1080">1080</option>
                                        <option value="1110">1110</option>
                                        <option value="1140">1140</option>
                                        <option value="1170">1170</option>
                                        <option value="1200">1200</option>
                                        <option value="1230">1230</option>
                                        <option value="1260">1260</option>
                                        <option value="1290">1290</option>
                                        <option value="1320">1320</option>
                                        <option value="1350">1350</option>
                                        <option value="1380">1380</option>
                                        <option value="1410">1410</option>
                                        <option value="1440">1440</option>
                                        <option value="1470">1470</option>
                                        <option value="1500">1500</option>
                                        <option value="1530">1530</option>
                                        <option value="1560">1560</option>
                                        <option value="1590">1590</option>
                                        <option value="1620">1620</option>

                                    </select> -->
                                    HARI DARI PENJUALAN, KELOMPOK
                                    <select name="ReminderForm[kelompok]" id="select-kelompok-<?php echo $value->id ?>" class="form-control d-inline-block align-middle mr-3" style="max-width: 12%; padding: 0.3rem;">
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>

                                    </select>
                                </div>
                            </div>
                        </div>
                        <script>
                            $(document).ready(function(){
                                // $('#select-hari-ke-<?php echo $value->id ?>').val('<?php echo $value->hari_ke ?>')
                                $('#select-kelompok-<?php echo $value->id ?>').val('<?php echo $value->kelompok ?>')
                            })
                        </script>
                        <div class="col-md-4">
                            <p>&nbsp;</p><br>
                            <div class="form-check form-check-inline align-middle pt-1">
                            <label class="form-check-label status" for="inlineRadio1">STATUS</label>
                            <input type="radio" id="pesanform-is_active" <?php if ($model->is_active == '1') { ?>checked="checked"<?php } ?> name="ReminderForm[is_active]" value="1" aria-invalid="false">
                            <label class="form-check-label" for="inlineRadio1">AKTIF</label>
                            </div>
                            <div class="form-check form-check-inline align-middle pt-1">
                            <input type="radio" id="pesanform-is_active" <?php if ($model->is_active == '0') { ?>checked="checked"<?php } ?> name="ReminderForm[is_active]" value="0" aria-invalid="false">
                            <label class="form-check-label" for="inlineRadio2">DELETE</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="content">
                        <?php
                        $model->pesan = $value->pesan;
                        ?>
                        <?= $form->field($model, 'pesan')->textarea([
                            'style'=>"width: 100%; border: none;",
                            'placeholder'=>"Isi pesan reminder di sini",
                            'rows' => 6
                        ]) ?>
                    </div>
                </div>
            </div>
            <div class="row pt-3 no-gutters">
                <div class="col-md-8">
                    
                </div>
                <div class="col-md-4 text-right">
                    <button class="btn btn-info">SIMPAN PESAN</button>
                </div>
            </div>
            <p>&nbsp;</p>
            <?php ActiveForm::end(); ?>
            <?php } ?>
            <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
            <input type="hidden" name="ReminderForm[id]" value="">
            <div class="card">
                <div class="card-header">
                    <div class="row atas-sendiri">
                        <div class="col-md-8">
                            <div class="template">
                                <p><span><img src="<?php echo \Yii::getAlias('@web').'/asset/images/icons-plancs.png' ?>" alt=""></span>TEMPLATE UCAPAN REMINDER KEPADA PENJUAL</p>
                                <div class="pl-5 ml-3">
                                    <label for="" class="d-inline align-middle">KIRIM KETIKA</label> &nbsp;&nbsp;&nbsp;
                                    <input type="text" name="ReminderForm[hari_ke]" class="form-control d-inline-block align-middle mr-3" style="max-width: 12%; padding: 0.3rem;">
                                    <!-- <select name="ReminderForm[hari_ke]" class="form-control d-inline-block align-middle mr-3" style="max-width: 12%; padding: 0.3rem;">
                                        <option value="1">1</option>
                                        <option value="7">7</option>
                                        <option value="14">14</option>
                                        <option value="21">21</option>
                                        <option value="30">30</option>
                                        <option value="60">60</option>
                                        <option value="90">90</option>
                                        <option value="120">120</option>
                                        <option value="150">150</option>
                                        <option value="180">180</option>
                                        <option value="210">210</option>
                                        <option value="240">240</option>
                                        <option value="270">270</option>
                                        <option value="300">300</option>
                                        <option value="330">330</option>
                                        <option value="360">360</option>
                                        <option value="390">390</option>
                                        <option value="420">420</option>
                                        <option value="450">450</option>
                                        <option value="480">480</option>
                                        <option value="510">510</option>
                                        <option value="540">540</option>
                                        <option value="570">570</option>
                                        <option value="600">600</option>
                                        <option value="630">630</option>
                                        <option value="660">660</option>
                                        <option value="690">690</option>
                                        <option value="720">720</option>
                                        <option value="750">750</option>
                                        <option value="780">780</option>
                                        <option value="810">810</option>
                                        <option value="840">840</option>
                                        <option value="870">870</option>
                                        <option value="900">900</option>
                                        <option value="930">930</option>
                                        <option value="960">960</option>
                                        <option value="990">990</option>
                                        <option value="1020">1020</option>
                                        <option value="1050">1050</option>
                                        <option value="1080">1080</option>
                                        <option value="1110">1110</option>
                                        <option value="1140">1140</option>
                                        <option value="1170">1170</option>
                                        <option value="1200">1200</option>
                                        <option value="1230">1230</option>
                                        <option value="1260">1260</option>
                                        <option value="1290">1290</option>
                                        <option value="1320">1320</option>
                                        <option value="1350">1350</option>
                                        <option value="1380">1380</option>
                                        <option value="1410">1410</option>
                                        <option value="1440">1440</option>
                                        <option value="1470">1470</option>
                                        <option value="1500">1500</option>
                                        <option value="1530">1530</option>
                                        <option value="1560">1560</option>
                                        <option value="1590">1590</option>
                                        <option value="1620">1620</option>

                                    </select> -->
                                    HARI DARI PENJUALAN, KELOMPOK
                                    <select name="ReminderForm[kelompok]" class="form-control d-inline-block align-middle mr-3" style="max-width: 12%; padding: 0.3rem;">
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>

                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <p>&nbsp;</p><br>
                            <div class="form-check form-check-inline align-middle pt-1">
                            <label class="form-check-label status" for="inlineRadio1">STATUS</label>
                            <input type="radio" id="pesanform-is_active" checked="checked" name="ReminderForm[is_active]" value="1" aria-invalid="false">
                            <label class="form-check-label" for="inlineRadio1">AKTIF</label>
                            </div>
                            <div class="form-check form-check-inline align-middle pt-1">
                            <input type="radio" id="pesanform-is_active" name="ReminderForm[is_active]" value="0" aria-invalid="false">
                            <label class="form-check-label" for="inlineRadio2">DELETE</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="content">
                        <?php
                        $model->pesan = '';
                        ?>
                        <?= $form->field($model, 'pesan')->textarea([
                            'style'=>"width: 100%; border: none;",
                            'placeholder'=>"Isi pesan reminder di sini",
                            'rows' => 6
                        ]) ?>
                    </div>
                </div>
            </div>
            <div class="row pt-3 no-gutters">
                <div class="col-md-8">
                    
                </div>
                <div class="col-md-4 text-right">
                    <button class="btn btn-info">SIMPAN PESAN</button>
                </div>
            </div>
            <p>&nbsp;</p>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>

<style type="text/css">
    .birthday .row .card .card-header .template p{
        font-size: 18px;
    }
</style>