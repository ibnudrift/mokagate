<?php
use yii\widgets\ActiveForm;
use yii\helpers\Url;
?>
<div class="birthday">
    <div class="row no-gutters">
        <div class="col-md-1">
            <img src="<?php echo \Yii::getAlias('@web').'/asset/images/reload.png' ?>" alt="">
        </div>
        <div class="col-md-11 blocks_rights_dvn">
        <?php // $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
            <div class="kirim-pesan">
                <a href="#">
                    <p>ANTRIAN PESAN</p>
                </a>
            </div>
            <?php if (Yii::$app->session->hasFlash('success')): ?>
                <div class="alert alert-success alert-dismissable">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <h4><i class="icon fa fa-check"></i>Sukses!</h4>
                    <?= Yii::$app->session->getFlash('success') ?>
                </div>
            <?php endif; ?>
            <?php if (Yii::$app->session->hasFlash('error')): ?>
                <div class="alert alert-danger alert-dismissable">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <h4><i class="icon fa fa-check"></i>Failed!</h4>
                    <?= Yii::$app->session->getFlash('error') ?>
                </div>
            <?php endif; ?>
            <div class="py-1"></div>
            <a href="<?php echo Url::toRoute(['/home/deleteall']) ?>" class="btn btn-customs_deforange">DELETE SEMUA ANTRIAN</a>&nbsp;&nbsp;
            <a href="<?php echo Url::toRoute(['/home/exeantrian']) ?>" class="btn btn-customs_deforange">KIRIM (ALL)</a>
            <div class="py-2"></div>
            
            <table class="table customs_table table-striped">
              <thead class="thead-dark">
                <tr>
                  <th scope="col">No WA</th>
                  <th scope="col">Pesan</th>
                </tr>
              </thead>
              <tbody>
              <?php foreach ($data as $key => $value) { ?>
                <tr>
                  <td><?php echo $value->no_wa ?></td>
                  <td><?php echo nl2br($value->pesan) ?></td>
                </tr>
              <?php } ?>
              </tbody>
            </table>


            <div class="clear clearfix"></div>
        <?php // ActiveForm::end(); ?>
        </div>
    </div>
</div>

