<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
?>
<div class="insides-pg inners_page_editing">
	<div class="top-inner">
		
	</div>

	<div class="content inners_pglist_car insert_data">
		<div class="inners-top-status-mobil">
			<div class="inns_top_leftTops pt-3 pb-4">
				<span>DATA KENDARAAN & DATA PENJUAL</span>
			</div>
			<?php if (isset($errors)): ?>
				<div class="alert alert-warning" role="alert">
				  <?php foreach ($errors as $key => $val_error): ?>
				  	<?php echo $val_error; ?>
				  <?php endforeach ?>
				</div>
			<?php endif ?>
			<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
			<div class="btm_pembelians pt-3 box-form-widget">
				<p class="pb-3"><span>DATA PEMBELIAN</span></p>
				<div class="row pb-3">
					<div class="col-md-6">
    					<div class="input-wrap form-group">
			                <div class="clearfix" id="UserLogin-gender">
			                    <?=
			                    $form->field($model, 'status_terjual', 
			                    	[
			                    		'template' => "<div class='form-group row'>{label}\n<div class='col-md-8'>{input}</div>\n{hint}\n{error}</div>",
			                    		'labelOptions' => [ 'class' => 'col-sm-4 col-form-label'],
			                    	]
			                		)
			                        ->radioList(
			                            [1 => 'SUDAH TERJUAL', 2 => 'BELUM TERJUAL'],
			                            [
			                                'item' => function($index, $label, $name, $checked, $value) {

			                                    $return = '<div class="custom-control custom-radio custom-control-inline">';
			                                    $return .= '<input type="radio" name="' . $name . '" value="' . $value . '" id="customRadioInline'.$index.'" class="custom-control-input">';
			                                    // $return .= '<i></i>';
			                                    $return .= '<label class="custom-control-label" for="customRadioInline'.$index.'">' . ucwords($label) . '</label>';
			                                    $return .= '</div>';
			                                    return $return;
			                                }
			                            ]
			                        )->label('STATUS TERJUAL');
			                    ?>
			                </div>
			                <div class="help-block"></div>
			            </div>
			            <?= $form->field($modelPembelian, 'tanggal_penjualan', [
					                    'template' => "<div class='form-group row'>{label}\n<div class='col-md-5'>{input}</div>\n{hint}\n{error}</div>",
					                    'labelOptions' => [ 'class' => 'col-sm-4 col-form-label' ],
					                    'inputOptions' => [ 'class' => 'form-control datepicker' ]
					    ])->textInput(['maxlength' => true])?>
					    <?= $form->field($modelPembelian, 'nama_pembeli', [
					                    'template' => "<div class='form-group row'>{label}\n<div class='col-md-8'>{input}</div>\n{hint}\n{error}</div>",
					                    'labelOptions' => [ 'class' => 'col-sm-4 col-form-label' ],
					                    'inputOptions' => [ 'class' => 'form-control' ]
					    ])->textInput(['maxlength' => true])?>
					    <?= $form->field($modelPembelian, 'alamat_pembeli', [
					                    'template' => "<div class='form-group row'>{label}\n<div class='col-md-8'>{input}</div>\n{hint}\n{error}</div>",
					                    'labelOptions' => [ 'class' => 'col-sm-4 col-form-label' ],
					                    'inputOptions' => [ 'class' => 'form-control' ]
					    ])->textInput(['maxlength' => true])?>
					    <?= $form->field($modelPembelian, 'no_hp_pembeli', [
					                    'template' => "<div class='form-group row'>{label}\n<div class='col-md-8'>{input}</div>\n{hint}\n{error}</div>",
					                    'labelOptions' => [ 'class' => 'col-sm-4 col-form-label' ],
					                    'inputOptions' => [ 'class' => 'form-control' ]
					    ])->textInput(['maxlength' => true])?>
					    <div class="input-wrap form-group">
			                <div class="clearfix" id="choose_payment">
			                    <?=
			                    $form->field($modelPembelian, 'jenis_beli', 
			                    	[
			                    		'template' => "<div class='form-group row'>{label}\n<div class='col-md-8'>{input}</div>\n{hint}\n{error}</div>",
			                    		'labelOptions' => [ 'class' => 'col-sm-4 col-form-label'],
			                    	]
			                		)
			                        ->radioList(
			                            [1 => 'CASH', 2 => 'KREDIT'],
			                            [
			                                'item' => function($index, $label, $name, $checked, $value) {

			                                    $return = '<div class="custom-control custom-radio custom-control-inline">';
			                                    $return .= '<input type="radio" name="' . $name . '" value="' . $value . '" id="customRadioInli'.$index.'" class="custom-control-input">';
			                                    // $return .= '<i></i>';
			                                    $return .= '<label class="custom-control-label" for="customRadioInli'.$index.'">' . ucwords($label) . '</label>';
			                                    $return .= '</div>';
			                                    return $return;
			                                }
			                            ]
			                        )->label('JENIS BELI');
			                    ?>
			                </div>
			                <div class="help-block"></div>
			            </div>

			            <?= $form->field($modelPembelian, 'perusahaan_kredit', [
					                    'template' => "<div class='form-group row'><label class='col-sm-4 col-form-label'>&nbsp;</label><div class='col-md-8'><div class='row no-gutters'>{label}\n{input}</div></div>\n{hint}\n{error}</div>",
					                    'labelOptions' => [ 'class' => 'col-sm-5 col-form-label' ],
					                    'inputOptions' => [ 'class' => 'form-control col-sm-7', 'disabled'=>'disabled' ]
					    ])->textInput(['maxlength' => true])?>
					    <?= $form->field($modelPembelian, 'tanggal_lunas', [
					                    'template' => "<div class='form-group row'><label class='col-sm-4 col-form-label'>&nbsp;</label><div class='col-md-8'><div class='row no-gutters'>{label}\n{input}</div></div>\n{hint}\n{error}</div>",
					                    'labelOptions' => [ 'class' => 'col-sm-5 col-form-label' ],
					                    'inputOptions' => [ 'class' => 'form-control col-sm-7 datepicker_lunas', 'disabled'=>'disabled' ]
					    ])->textInput(['maxlength' => true])?>
					    <?= $form->field($modelPembelian, 'nominal_angsuran', [
					                    'template' => "<div class='form-group row'><label class='col-sm-4 col-form-label'>&nbsp;</label><div class='col-md-8'><div class='row no-gutters'>{label}\n{input}</div></div>\n{hint}\n{error}</div>",
					                    'labelOptions' => [ 'class' => 'col-sm-5 col-form-label' ],
					                    'inputOptions' => [ 'class' => 'form-control col-sm-7', 'disabled'=>'disabled' ]
					    ])->textInput(['maxlength' => true])?>
					    <div class="py-1"></div>
					    <?= $form->field($modelPembelian, 'harga_jual', [
					                    'template' => "<div class='form-group row'>{label}\n<div class='col-md-8'>{input}</div>\n{hint}\n{error}</div>",
					                    'labelOptions' => [ 'class' => 'col-sm-4 col-form-label' ],
					                    'inputOptions' => [ 'class' => 'form-control' ]
					    ])->textInput(['maxlength' => true])?>
					    <?= $form->field($modelPembelian, 'tanggal_lahir_pembeli', [
					                    'template' => "<div class='form-group row'>{label}\n<div class='col-md-8'>{input}</div>\n{hint}\n{error}</div>",
					                    'labelOptions' => [ 'class' => 'col-sm-4 col-form-label' ],
					                    'inputOptions' => [ 'class' => 'form-control datepicker2' ]
					    ])->textInput(['maxlength' => true])?>

					</div>
					<div class="col-md-6">
						<div class='form-group row'>
							<label class="col-sm-4 col-form-label">DOKUMEN</label>
							<div class='col-md-8 d-none d-sm-block'>&nbsp;</div>
						</div>
						<?php 
						$lsn_ar_right1 = [
										'dok_ktp_pembeli',
										'dok_foto_pembeli',
										];
						?>
						
						<?php foreach ($lsn_ar_right1 as $key => $value): ?>
						<?= $form->field($modelPembelian, $value, [
					                    'template' => "<div class='form-group row'>{label}\n<div class='col-md-8'>{input}</div>\n{hint}\n{error}</div>",
					                    'labelOptions' => [ 'class' => 'col-sm-4 col-form-label' ],
					                    'inputOptions' => [ 'class'=>'form-control' ]
					    ])->fileInput(); ?>
					    <?php if (!empty($modelPembelian->{$value})): ?>
					    <div class="form-group row">
					    	<div class="col-sm-4"></div>
					    	<div class="col-md-8 blocks_thumbnmag">
					    		<a data-fancybox="fancybox" href="<?php echo Yii::getAlias('@web').'/uploads/mobil/'.$modelPembelian->{$value} ?>">
					    			<img src="<?php echo Yii::getAlias('@web').'/uploads/mobil/'.$modelPembelian->{$value} ?>" alt="" class="img img-fluid img-thumbnail rounded">
					    		</a>
					    	</div>
					    </div>
					    <?php endif ?>
						<?php endforeach ?>

						<div class="py-1"></div>
						<?php if ( $modelPembelian->nama_pembeli == '' ): ?>
						<div class="form-group">
					        <?php echo Html::submitButton('UPDATE DATA', ['class' => 'btn btn-primary btns_submitn_frmyellow']) ?>
					    </div>
						<?php endif; ?>
					</div>
				</div>
				<div class="clear clearfix"></div>
			</div>

			<div class="clear clearfix"></div>
		</div>

		<div class="py-1"></div>
		<div class="lines-white d-block my-4"></div>
		<div class="py-1"></div>
		<div class="box-form-widget pt-3">
		<div class="n innerstop mb-4">
			<span>DATA KENDARAAN & DATA PENJUAL</span>
		</div>
			<div class="row">
				<div class="col-md-6">
			 		<?= $form->field($model, 'tgl_beli', [
				                    'template' => "<div class='form-group row'>{label}\n<div class='col-md-5'>{input}</div>\n{hint}\n{error}</div>",
				                    'labelOptions' => [ 'class' => 'col-sm-4 col-form-label' ],
				                    'inputOptions' => [ 'class' => 'form-control datepicker', 'readonly'=>'readonly' ]
				    ])->textInput(['maxlength' => true])?>
					<?= $form->field($model, 'no_polisi', [
				                    'template' => "<div class='form-group row'>{label}\n<div class='col-md-8'>{input}</div>\n{hint}\n{error}</div>",
				                    'labelOptions' => [ 'class' => 'col-sm-4 col-form-label' ],
				                    'inputOptions' => [ 'class' => 'form-control', 'readonly'=>'readonly' ]
				    ])->textInput(['maxlength' => true])?>

				    <?= $form->field($model, 'atas_nama_stnk', [
				                    'template' => "<div class='form-group row'>{label}\n<div class='col-md-8'>{input}</div>\n{hint}\n{error}</div>",
				                    'labelOptions' => [ 'class' => 'col-sm-4 col-form-label' ],
				                    'inputOptions' => [ 'class' => 'form-control', 'readonly'=>'readonly' ]
				    ])->textInput(['maxlength' => true])?>
				    <?= $form->field($model, 'alamat_stnk', [
				                    'template' => "<div class='form-group row'>{label}\n<div class='col-md-8'>{input}</div>\n{hint}\n{error}</div>",
				                    'labelOptions' => [ 'class' => 'col-sm-4 col-form-label' ],
				                    'inputOptions' => [ 'class' => 'form-control', 'readonly'=>'readonly' ]
				    ])->textInput(['maxlength' => true])?>
				    <?= $form->field($model, 'no_rangka', [
				                    'template' => "<div class='form-group row'>{label}\n<div class='col-md-8'>{input}</div>\n{hint}\n{error}</div>",
				                    'labelOptions' => [ 'class' => 'col-sm-4 col-form-label' ],
				                    'inputOptions' => [ 'class' => 'form-control', 'readonly'=>'readonly' ]
				    ])->textInput(['maxlength' => true])?>
				    <?= $form->field($model, 'no_mesin', [
				                    'template' => "<div class='form-group row'>{label}\n<div class='col-md-8'>{input}</div>\n{hint}\n{error}</div>",
				                    'labelOptions' => [ 'class' => 'col-sm-4 col-form-label' ],
				                    'inputOptions' => [ 'class' => 'form-control', 'readonly'=>'readonly' ]
				    ])->textInput(['maxlength' => true])?>
				    <?= $form->field($model, 'jenis_kendaraan', [
				                    'template' => "<div class='form-group row'>{label}\n<div class='col-md-8'>{input}</div>\n{hint}\n{error}</div>",
				                    'labelOptions' => [ 'class' => 'col-sm-4 col-form-label' ],
				                    'inputOptions' => [ 'class' => 'form-control', 'readonly'=>'readonly' ]
				    ])->textInput(['maxlength' => true])?>
				    <?= $form->field($model, 'warna', [
				                    'template' => "<div class='form-group row'>{label}\n<div class='col-md-8'>{input}</div>\n{hint}\n{error}</div>",
				                    'labelOptions' => [ 'class' => 'col-sm-4 col-form-label' ],
				                    'inputOptions' => [ 'class' => 'form-control', 'readonly'=>'readonly' ]
				    ])->textInput(['maxlength' => true])?>
				    <?= $form->field($model, 'tahun_kendaraan', [
				                    'template' => "<div class='form-group row'>{label}\n<div class='col-md-8'>{input}</div>\n{hint}\n{error}</div>",
				                    'labelOptions' => [ 'class' => 'col-sm-4 col-form-label' ],
				                    'inputOptions' => [ 'class' => 'form-control', 'readonly'=>'readonly' ]
				    ])->textInput(['maxlength' => true])?>
				    <?= $form->field($model, 'tanggal_berlaku_pajak', [
				                    'template' => "<div class='form-group row'>{label}\n<div class='col-md-8'>{input}</div>\n{hint}\n{error}</div>",
				                    'labelOptions' => [ 'class' => 'col-sm-4 col-form-label' ],
				                    'inputOptions' => [ 'class' => 'form-control datepicker', 'readonly'=>'readonly' ]
				    ])->textInput(['maxlength' => true])?>
				    

				</div>
				<div class="col-md-6">
					<?= $form->field($model, 'nama_penjual', [
				                    'template' => "<div class='form-group row'>{label}\n<div class='col-md-8'>{input}</div>\n{hint}\n{error}</div>",
				                    'labelOptions' => [ 'class' => 'col-sm-4 col-form-label' ],
				                    'inputOptions' => [ 'class' => 'form-control', 'readonly'=>'readonly' ]
				    ])->textInput(['maxlength' => true])?>
				    <?= $form->field($model, 'hp_penjual', [
				                    'template' => "<div class='form-group row'>{label}\n<div class='col-md-8'>{input}</div>\n{hint}\n{error}</div>",
				                    'labelOptions' => [ 'class' => 'col-sm-4 col-form-label' ],
				                    'inputOptions' => [ 'class' => 'form-control', 'readonly'=>'readonly' ]
				    ])->textInput(['maxlength' => true])?>
				    <?= $form->field($model, 'alamat_penjual', [
				                    'template' => "<div class='form-group row'>{label}\n<div class='col-md-8'>{input}</div>\n{hint}\n{error}</div>",
				                    'labelOptions' => [ 'class' => 'col-sm-4 col-form-label' ],
				                    'inputOptions' => [ 'class' => 'form-control', 'readonly'=>'readonly' ]
				    ])->textInput(['maxlength' => true])?>
				    <?= $form->field($model, 'tanggal_lahir_penjual', [
				                    'template' => "<div class='form-group row'>{label}\n<div class='col-md-8'>{input}</div>\n{hint}\n{error}</div>",
				                    'labelOptions' => [ 'class' => 'col-sm-4 col-form-label' ],
				                    'inputOptions' => [ 'class' => 'form-control datepicker', 'readonly'=>'readonly' ]
				    ])->textInput(['maxlength' => true])?>
				    <?= $form->field($model, 'total_perbaikan', [
				                    'template' => "<div class='form-group row'>{label}\n<div class='col-md-8'>{input}</div>\n{hint}\n{error}</div>",
				                    'labelOptions' => [ 'class' => 'col-sm-4 col-form-label' ],
				                    'inputOptions' => [ 'class' => 'form-control', 'readonly'=>'readonly' ]
				    ])->textInput(['maxlength' => true])?>
				    <?= $form->field($model, 'harga_beli', [
				                    'template' => "<div class='form-group row'>{label}\n<div class='col-md-8'>{input}</div>\n{hint}\n{error}</div>",
				                    'labelOptions' => [ 'class' => 'col-sm-4 col-form-label' ],
				                    'inputOptions' => [ 'class' => 'form-control', 'readonly'=>'readonly' ]
				    ])->textInput(['maxlength' => true])?>

					<div class='form-group row'>
						<label class="col-sm-4 col-form-label">UPLOAD DOKUMEN</label>
						<div class="col-md-8 mx-auto">
					    	<?php if (count($modelDokumen) <= 0): ?>
					        <!-- empty -->
					    	<?php else: ?>
					    	<?php foreach ($modelDokumen as $key => $value): ?>
					    		<?php if ($key <= 0): ?>
					        		<a href="<?php echo Yii::getAlias('@web').'/uploads/dokumen/'.$value->images ?>" data-fancybox="images" class="btn btn-secondary butns_ingrey">LIHAT</a>
					    		<?php else: ?>
					    			<a href="<?php echo Yii::getAlias('@web').'/uploads/dokumen/'.$value->images ?>" data-fancybox="images"></a>
					    		<?php endif ?>
					    	<?php endforeach ?>
					    	<?php endif ?>
					    </div>
					</div>
					<?php 
					/*$lsn_ar_right = [
									'dok_stnk',
									'dok_bpkb',
									'dok_faktur',
									'dok_copyktp_stnk',
									'dok_surat_buka_blokir',
									'dok_pelepasan_hak',
									'dok_kwitansi_blangko',
									'dok_ktp_penjual',
									'dok_foto_penjual',
									'dok_nota_bon1',
									'dok_nota_bon2',
									'dok_nota_bon3',
									'dok_nota_bon4',
									'dok_nota_bon5',
									];
					?>
					<?php foreach ($lsn_ar_right as $key => $value): ?>
					<div class="form-group row">
					    <label class="col-sm-4 col-form-label" for="mastermobil-dok_stnk"><?= Html::activeLabel($model, $value); ?></label>
					    <div class="col-md-8 mx-auto text-center">
					    	<?php if ($model->{$value} == '' OR $model->{$value} == null): ?>
					        -
					    	<?php else: ?>
					        <a href="<?php echo Yii::getAlias('@web').'/uploads/mobil/'.$model->{$value} ?>" data-fancybox="images" class="btn btn-secondary butns_ingrey">LIHAT</a>
					    	<?php endif ?>
					    </div>
					</div>
					<?php endforeach*/ ?>

					<div class="clearfix clear"></div>
				</div>
			</div>
			<div class="py-2"></div>
			<div class="row">
				<div class="col-md-6"></div>
				<div class="col-md-6">
					<div class='form-group row d-none d-sm-block'>
						<label class="col-sm-8 col-form-label">&nbsp;</label>
						<div class='col-md-4'>&nbsp;</div>
					</div>

					<div class="form-group row">
					    <label class="col-sm-4 col-form-label" for="mastermobil-dok_stnk">GALLERY PHOTO KENDARAAN</label>
					    <div class="col-md-8 mx-auto">
					    	<?php if (count($modelGallery) <= 0): ?>
					        <!-- empty -->
					    	<?php else: ?>
					    	<?php foreach ($modelGallery as $key => $value): ?>
					    		<?php if ($key <= 0): ?>
					        		<a href="<?php echo Yii::getAlias('@web').'/uploads/gallery/'.$value->images ?>" data-fancybox="images" class="btn btn-secondary butns_ingrey">LIHAT</a>
					    		<?php else: ?>
					    			<a href="<?php echo Yii::getAlias('@web').'/uploads/gallery/'.$value->images ?>" data-fancybox="images"></a>
					    		<?php endif ?>
					    	<?php endforeach ?>
					    	<?php endif ?>
					    </div>
					</div>

					<div class="clearfix"></div>
				</div>
			</div>
			<div class="py-2"></div>
			<div class="row d-none">
				<div class="col-md-6"></div>
				<div class="col-md-6">
				    <div class="form-group">
				        <?php echo Html::submitButton('UPDATE DATA', ['class' => 'btn btn-primary btns_submitn_frmyellow']) ?>
				    </div>
				</div>
			</div>
		</div>
		<div class="clear"></div>
		<?php ActiveForm::end(); ?>
	</div>
	<div class="clearfix"></div>
</div>

<script type="text/javascript">
	$(function(){
		<?php if ( isset($modelPembelian) AND $modelPembelian->nama_pembeli != '' ): ?>
		var selctn_1 = $('input#customRadioInline0');
		var selctn_2 = $('input#customRadioInline1');

		var check_status = <?php echo $model->status_terjual; ?>;
		if (check_status == "1"){
			$(selctn_1).attr('checked', 'checked');
		} else {
			$(selctn_2).attr('checked', 'checked');
		}

		$('.custom-control-inline.custom-radio').find('input').attr('disabled', 'disabled');
		
		var selctn_p1 = $('input#customRadioInli0');
		var selctn_p2 = $('input#customRadioInli1');

		$('input#masterpembelian-tanggal_penjualan, input#masterpembelian-nama_pembeli, input#masterpembelian-alamat_pembeli, input#masterpembelian-no_hp_pembeli, input#masterpembelian-harga_jual, input#masterpembelian-tanggal_lahir_pembeli').attr('readonly', 'readonly');


		var check_statusp = <?php echo $modelPembelian->jenis_beli; ?>;
		if (check_statusp == "2"){
			$(selctn_p2).attr('checked', 'checked');
		} else {
			$(selctn_p1).attr('checked', 'checked');
		}
		<?php else: ?>

			$('#choose_payment .custom-control input[type=radio]').change(function(){
		  		var s_pkredit = $('#masterpembelian-perusahaan_kredit');
		  		var s_tlunas = $('#masterpembelian-tanggal_lunas');
		  		var s_nangsuran = $('#masterpembelian-nominal_angsuran');
		  		var n_val = $(this).val();
		  		// alert("asdfasdf - "+ n_val); return false;
		  		if (n_val == "2"){
		  			$(s_pkredit).removeAttr('disabled');
		  			$(s_tlunas).removeAttr('disabled');
		  			$(s_nangsuran).removeAttr('disabled');
		  		}else{
		  			$(s_pkredit).val('').attr('disabled', 'disabled');
		  			$(s_tlunas).val('').attr('disabled', 'disabled');
		  			$(s_nangsuran).val('').attr('disabled', 'disabled');
		  		}
		  		return true;
		  	});

		<?php endif ?>		

		return false;
	});
</script>
<style type="text/css">
	.blocks_thumbnmag img{
		max-width: 110px;
	}
</style>