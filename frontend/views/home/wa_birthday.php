<?php
use yii\widgets\ActiveForm;
?>
<div class="birthday">
    <div class="row no-gutters">
        <div class="col-md-1">
            <img src="<?php echo \Yii::getAlias('@web').'/asset/images/reload.png' ?>" alt="">
        </div>
        <div class="col-md-11">
            <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
            <div class="kirim-pesan">
                <a href="#">
                    <p>KIRIM PESAN WA</p>
                </a>
            </div>
            <?php if (Yii::$app->session->hasFlash('success')): ?>
                <div class="alert alert-success alert-dismissable">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <h4><i class="icon fa fa-check"></i>Saved!</h4>
                    <?= Yii::$app->session->getFlash('success') ?>
                </div>
            <?php endif; ?>
            <?php if (Yii::$app->session->hasFlash('error')): ?>
                <div class="alert alert-danger alert-dismissable">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <h4><i class="icon fa fa-check"></i>Failed!</h4>
                    <?= Yii::$app->session->getFlash('error') ?>
                </div>
            <?php endif; ?>
            <?php
            if($model->hasErrors()){
                echo Html::errorSummary($model, [
                    'class'=>'alert alert-danger alert-dismissable',
                ]);
            }
            ?>
            <div class="card">
                <div class="card-header">
                    <div class="row atas-sendiri">
                        <div class="col-md-8">
                            <div class="template">
                                <p><span><img src="<?php echo \Yii::getAlias('@web').'/asset/images/birth.png' ?>" alt=""></span>TEMPLATE UCAPAN ULANG TAHUN</p>
                            </div>
                        </div>
                        <div class="col-md-4">
                        <div class="form-check form-check-inline">
                            <label class="form-check-label status" for="inlineRadio1">STATUS</label>
                            <input type="radio" id="pesanform-is_active" <?php if ($model->is_active == '1') { ?>checked="checked"<?php } ?> name="PesanForm[is_active]" value="1" aria-invalid="false">
                            <label class="form-check-label" for="inlineRadio1">AKTIF</label>
                            </div>
                            <div class="form-check form-check-inline">
                            <input type="radio" id="pesanform-is_active" <?php if ($model->is_active == '0') { ?>checked="checked"<?php } ?> name="PesanForm[is_active]" value="0" aria-invalid="false">
                            <label class="form-check-label" for="inlineRadio2">NON AKTIF</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="content">
                        <?= $form->field($model, 'pesan')->textarea([
                            'style'=>"width: 100%; border: none;",
                            'placeholder'=>"Isi pesan ulang tahun di sini",
                            'rows' => 6
                        ]) ?>
                    </div>
                </div>
            </div>
            <div class="row pt-3 no-gutters">
                <div class="col-md-8">
                    <div class="bawah-card">
                        <p>Kode [NAMA] akan otomatis tergantikan nama pelanggan ketika pesan dikirim.</p>
                    </div>
                </div>
                <div class="col-md-4 text-right">
                    <button type="submit" class="btn btn-info">UPDATE TEMPLATE</button>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>

