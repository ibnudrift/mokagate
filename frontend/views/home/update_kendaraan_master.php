<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>
<div class="insides-pg inners_page_editing">
	<div class="top-inner">
		
	</div>

	<div class="content inners_pglist_car insert_data">
		<div class="inns_top_leftTops pt-3 pb-4">
			<span>KENDARAAN UPDATE</span>
			<small>UPDATE RECORD VEHICLE</small>
		</div>
		<div class="box-form-widget pt-3">
		<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
			<div class="row">
				<div class="col-md-6">
			 		<?= $form->field($model, 'tgl_beli', [
				                    'template' => "<div class='form-group row'>{label}\n<div class='col-md-5'>{input}</div>\n{hint}\n{error}</div>",
				                    'labelOptions' => [ 'class' => 'col-sm-4 col-form-label' ],
				                    'inputOptions' => [ 'class' => 'datepicker form-control' ]
				    ])->textInput(['maxlength' => true])?>
					<?= $form->field($model, 'no_polisi', [
				                    'template' => "<div class='form-group row'>{label}\n<div class='col-md-8'>{input}</div>\n{hint}\n{error}</div>",
				                    'labelOptions' => [ 'class' => 'col-sm-4 col-form-label' ],
				                    'inputOptions' => [ 'class' => 'form-control' ]
				    ])->textInput(['maxlength' => true])?>

				    <?= $form->field($model, 'atas_nama_stnk', [
				                    'template' => "<div class='form-group row'>{label}\n<div class='col-md-8'>{input}</div>\n{hint}\n{error}</div>",
				                    'labelOptions' => [ 'class' => 'col-sm-4 col-form-label' ],
				                    'inputOptions' => [ 'class' => 'form-control' ]
				    ])->textInput(['maxlength' => true])?>
				    <?= $form->field($model, 'alamat_stnk', [
				                    'template' => "<div class='form-group row'>{label}\n<div class='col-md-8'>{input}</div>\n{hint}\n{error}</div>",
				                    'labelOptions' => [ 'class' => 'col-sm-4 col-form-label' ],
				                    'inputOptions' => [ 'class' => 'form-control' ]
				    ])->textInput(['maxlength' => true])?>
				    <?= $form->field($model, 'no_rangka', [
				                    'template' => "<div class='form-group row'>{label}\n<div class='col-md-8'>{input}</div>\n{hint}\n{error}</div>",
				                    'labelOptions' => [ 'class' => 'col-sm-4 col-form-label' ],
				                    'inputOptions' => [ 'class' => 'form-control' ]
				    ])->textInput(['maxlength' => true])?>
				    <?= $form->field($model, 'no_mesin', [
				                    'template' => "<div class='form-group row'>{label}\n<div class='col-md-8'>{input}</div>\n{hint}\n{error}</div>",
				                    'labelOptions' => [ 'class' => 'col-sm-4 col-form-label' ],
				                    'inputOptions' => [ 'class' => 'form-control' ]
				    ])->textInput(['maxlength' => true])?>
				    <?= $form->field($model, 'jenis_kendaraan', [
				                    'template' => "<div class='form-group row'>{label}\n<div class='col-md-8'>{input}</div>\n{hint}\n{error}</div>",
				                    'labelOptions' => [ 'class' => 'col-sm-4 col-form-label' ],
				                    'inputOptions' => [ 'class' => 'form-control' ]
				    ])->textInput(['maxlength' => true])?>
				    <?= $form->field($model, 'warna', [
				                    'template' => "<div class='form-group row'>{label}\n<div class='col-md-8'>{input}</div>\n{hint}\n{error}</div>",
				                    'labelOptions' => [ 'class' => 'col-sm-4 col-form-label' ],
				                    'inputOptions' => [ 'class' => 'form-control' ]
				    ])->textInput(['maxlength' => true])?>
				    <?= $form->field($model, 'tahun_kendaraan', [
				                    'template' => "<div class='form-group row'>{label}\n<div class='col-md-8'>{input}</div>\n{hint}\n{error}</div>",
				                    'labelOptions' => [ 'class' => 'col-sm-4 col-form-label' ],
				                    'inputOptions' => [ 'class' => 'form-control' ]
				    ])->textInput(['maxlength' => true])?>
				    <?= $form->field($model, 'tanggal_berlaku_pajak', [
				                    'template' => "<div class='form-group row'>{label}\n<div class='col-md-8'>{input}</div>\n{hint}\n{error}</div>",
				                    'labelOptions' => [ 'class' => 'col-sm-4 col-form-label' ],
				                    'inputOptions' => [ 'class' => 'form-control datepicker2' ]
				    ])->textInput(['maxlength' => true])?>
				    <?= $form->field($model, 'nama_penjual', [
				                    'template' => "<div class='form-group row'>{label}\n<div class='col-md-8'>{input}</div>\n{hint}\n{error}</div>",
				                    'labelOptions' => [ 'class' => 'col-sm-4 col-form-label' ],
				                    'inputOptions' => [ 'class' => 'form-control' ]
				    ])->textInput(['maxlength' => true])?>

				</div>
				<div class="col-md-6">
					
				    
				    <?= $form->field($model, 'hp_penjual', [
				                    'template' => "<div class='form-group row'>{label}\n<div class='col-md-8'>{input}</div>\n{hint}\n{error}</div>",
				                    'labelOptions' => [ 'class' => 'col-sm-4 col-form-label' ],
				                    'inputOptions' => [ 'class' => 'form-control' ]
				    ])->textInput(['maxlength' => true])?>
				    <?= $form->field($model, 'alamat_penjual', [
				                    'template' => "<div class='form-group row'>{label}\n<div class='col-md-8'>{input}</div>\n{hint}\n{error}</div>",
				                    'labelOptions' => [ 'class' => 'col-sm-4 col-form-label' ],
				                    'inputOptions' => [ 'class' => 'form-control' ]
				    ])->textInput(['maxlength' => true])?>
				    <?= $form->field($model, 'tanggal_lahir_penjual', [
				                    'template' => "<div class='form-group row'>{label}\n<div class='col-md-8'>{input}</div>\n{hint}\n{error}</div>",
				                    'labelOptions' => [ 'class' => 'col-sm-4 col-form-label' ],
				                    'inputOptions' => [ 'class' => 'datepicker form-control' ]
				    ])->textInput(['maxlength' => true])?>
				    <?= $form->field($model, 'total_perbaikan', [
				                    'template' => "<div class='form-group row'>{label}\n<div class='col-md-8'>{input}</div>\n{hint}\n{error}</div>",
				                    'labelOptions' => [ 'class' => 'col-sm-4 col-form-label' ],
				                    'inputOptions' => [ 'class' => 'form-control' ]
				    ])->textInput(['maxlength' => true])?>
				    <?= $form->field($model, 'harga_beli', [
				                    'template' => "<div class='form-group row'>{label}\n<div class='col-md-8'>{input}</div>\n{hint}\n{error}</div>",
				                    'labelOptions' => [ 'class' => 'col-sm-4 col-form-label' ],
				                    'inputOptions' => [ 'class' => 'form-control' ]
				    ])->textInput(['maxlength' => true])?>
				    
				    <div class="form-group row">
				    	<div class="col">
				    		<div class="line-divider"></div>
				    	</div>
				    </div>

					<div class='form-group row'>
						<label class="col-sm-4 col-form-label">UPLOAD DOKUMEN</label>
						<div class='col-md-8 d-none d-sm-block'>&nbsp;</div>
					</div>
					<?php 
					/*
					$lsn_ar_right = [
									'dok_stnk',
									'dok_bpkb',
									'dok_faktur',
									'dok_copyktp_stnk',
									'dok_surat_buka_blokir',
									'dok_pelepasan_hak',
									'dok_kwitansi_blangko',
									'dok_ktp_penjual',
									'dok_foto_penjual',
									'dok_nota_bon1',
									'dok_nota_bon2',
									'dok_nota_bon3',
									'dok_nota_bon4',
									'dok_nota_bon5',
									];
					?>
					<?php foreach ($lsn_ar_right as $key => $value): ?>
					<?php if ($key < 3): ?>
						<?= $form->field($model, $value, [
					                    'template' => "<div class='form-group row'>{label}\n<div class='col-md-8'>{input}</div>\n{hint}\n{error}</div>",
					                    'labelOptions' => [ 'class' => 'col-sm-4 col-form-label' ],
					                    'inputOptions' => [ 'required' => 'required' ]
					    ])->fileInput(); ?>
					<?php else: ?>
						<?= $form->field($model, $value, [
					                    'template' => "<div class='form-group row'>{label}\n<div class='col-md-8'>{input}</div>\n{hint}\n{error}</div>",
					                    'labelOptions' => [ 'class' => 'col-sm-4 col-form-label' ],
					                    'inputOptions' => [  ]
					    ])->fileInput(); ?>
					<?php endif ?>
					<?php endforeach; ?>
					*/ ?>

					<?= $form->field($model, 'imageDokumen[]', [
				                    'template' => "<div class='form-group row'>{label}\n<div class='col-md-8'>{input}</div>\n{hint}\n{error}</div>",
				                    'labelOptions' => [ 'class' => 'col-sm-4 col-form-label' ],
				                    'inputOptions' => [ 'multiple' => 'multiple' ]
				    ])->fileInput(); ?>
					<div class="form-group row">
						<div class="col">
							<ul id="sortable">
								<?php foreach ($modelDokumen as $key => $value): ?>
								<li class="ui-state-default">
									<img src="<?php echo Yii::getAlias('@web').'/uploads/dokumen/'.$value->images ?>"/>
									<a href="#" class="delete-gambar"><i class="fa fa-trash-o"></i></a>
									<input type="hidden" name="imageDokumen2[]" value="<?php  echo $value->images ?>">
								</li>
								<?php endforeach ?>
							</ul>
						</div>
					</div>

				    <div class="form-group row">
				    	<div class="col">
				    		<div class="line-divider"></div>
				    	</div>
				    </div>

				    <div class='form-group row'>
						<label class="col-sm-8 col-form-label">GALLERY PHOTO KENDARAAN</label>
						<div class='col-md-4'>&nbsp;</div>
					</div>
					<?php echo $form->field($model, 'imageFiles[]', [
				                    'template' => "<div class='form-group row'>{label}\n<div class='col-md-8'>{input}</div>\n{hint}\n{error}</div>",
				                    'labelOptions' => [ 'class' => 'col-sm-4 col-form-label' ],
				                    'inputOptions' => [ 'multiple' => 'multiple' ]
				    ])->fileInput(); ?>

				    <?php /*<!-- <div class="gambar-tempel"></div>
			    	<div class="gambar-add">
						<input type="file" id="mastermobil-imagefiles" name="MasterMobil[imageFiles][]" multiple="multiple" aria-invalid="false">
			    	</div>
			    	<div class="py-1"></div> -->*/ ?>
			    	<p class="help-block"><b>Note:</b> Image max width 1600px. Larger image will be automatically cropped.</p>
				    <style>
					#sortable { list-style-type: none; margin: 0; padding: 0; width: 100%; }
					#sortable li { margin: 5px 2.5%; float: left; width: 25%; text-align: center; }
					#sortable li img { width: 96%; border: 2px solid #ccc; }
					#sortable li img{
						max-height: 70px;
					}
					.help-block{
						color: #fff; font-size: 12px;
					}
					</style>
					<ul id="sortable">
						<?php foreach ($modelGallery as $key => $value): ?>
						<li class="ui-state-default">
							<img src="<?php echo Yii::getAlias('@web').'/uploads/gallery/'.$value->images ?>"/>
							<a href="#" class="delete-gambar"><i class="fa fa-trash-o"></i></a>
							<input type="hidden" name="MasterMobilFiles2[]" value="<?php  echo $value->images ?>">
						</li>
						<?php endforeach ?>
					</ul>
		            <script type="text/javascript">
		            jQuery(function( $ ) {
						$('.tambah-gambar').tambahData({
							targetHtml: '.gambar-add',
							// html: '<tr><td></td></tr>',
							tambahkan: '.gambar-tempel',
						});
						$( "#sortable" ).sortable();
						$( "#sortable" ).disableSelection();
						$(document).on('click', '.delete-gambar',function( e ) {
							$(this).parent().remove();
							return false;
						})
					})
		            </script>

					<div class="clearfix clear"></div>
				</div>
			</div>

			<div class="py-2"></div>
			<div class="row">
				<div class="col-md-6"></div>
				<div class="col-md-6">
				    <div class="form-group">
				        <?= Html::submitButton('Submit', ['class' => 'btn btn-primary btns_submitn_frmyellow']) ?>
				    </div>
				</div>
			</div>
			<?php ActiveForm::end(); ?>
		</div>
		<div class="clear"></div>
	</div>
	<div class="clearfix"></div>
</div>
