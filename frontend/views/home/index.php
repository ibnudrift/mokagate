<?php 
use yii\helpers\Url;

?>
<div class="dashboard-pg">
	
	<div class="row">
		<div class="col-md-6">
			<div class="box-black-widget">
				<div class="inner d-block my-auto text-center pt-5">
					<div class="icons"><img src="<?php echo \Yii::getAlias('@web').'/asset/images/nls_icon_home_1.jpg' ?>" alt="" class="img img-fluid"></div>
					<p>KENDARAAN BARU <br>
						<small>NEW RECORD VEHICLE</small></p>
					<div class="in-form">
					<a href="<?php echo Url::toRoute(['home/newvehicle']) ?>" class="btn btn-default">OK</a>
					</div>
				</div>
			</div>
			<div class="my-2"></div>
		</div>
		<div class="col-md-6">
			<div class="box-black-widget">
				<div class="inner d-block my-auto text-center pt-5">
					<div class="icons"><img src="<?php echo \Yii::getAlias('@web').'/asset/images/nls_icon_home_2.jpg' ?>" alt="" class="img img-fluid"></div>
					<p>CARI dan update data KENDARAAN<br>
						<small>Gunakan NO Polisi, NO MESIN, NO RANGKA nama, No HP</small></p>
					<div class="in-form">
						<form action="<?php echo Url::toRoute(['home/kendaraan']) ?>" method="get">
							<div class="form-group">
							<input type="text" class="form-control" name="search" value="">
							</div>
							<button class="btn btn-default">OK</button>
						</form>
					</div>
					<small>KOSONGKAN DAN TEKAN OK JIKA INGIN MELIHAT SEMUA DATA</small>
				</div>
			</div>
		</div>
	</div>

	<div class="clear clearfix"></div>
</div>