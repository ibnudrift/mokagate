<?php
use yii\widgets\ActiveForm;
?>
<div class="birthday">
    <div class="row no-gutters">
        <div class="col-md-1">
            <img src="<?php echo \Yii::getAlias('@web').'/asset/images/reload.png' ?>" alt="">
        </div>
        <div class="col-md-11">
        <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
            <div class="kirim-pesan">
                <a href="#">
                    <p>KIRIM PESAN WA</p>
                </a>
            </div>
            <?php if (Yii::$app->session->hasFlash('success')): ?>
                <div class="alert alert-success alert-dismissable">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <h4><i class="icon fa fa-check"></i>Saved!</h4>
                    <?= Yii::$app->session->getFlash('success') ?>
                </div>
            <?php endif; ?>
            <?php if (Yii::$app->session->hasFlash('error')): ?>
                <div class="alert alert-danger alert-dismissable">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <h4><i class="icon fa fa-check"></i>Failed!</h4>
                    <?= Yii::$app->session->getFlash('error') ?>
                </div>
            <?php endif; ?>
            <?php
            if($model->hasErrors()){
                echo Html::errorSummary($model, [
                    'class'=>'alert alert-danger alert-dismissable',
                ]);
            }
            ?>
            <div class="card">
                <div class="card-header">
                    <div class="row atas-sendiri">
                        <div class="col-md-8">
                            <div class="template">
                                <p><span><img src="<?php echo \Yii::getAlias('@web').'/asset/images/icons-plancs.png' ?>" alt=""></span>KIRIM PESAN BLAST</p>
                            </div>
                        </div>
                        <div class="col-md-4">
                        
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="content">
                        <?= $form->field($model, 'pesan')->textarea([
                            'style'=>"width: 100%; border: none;",
                            'placeholder'=>"Isi pesan yang akan di kirim ke semua kontak",
                            'rows' => 6
                        ]) ?>
                    </div>
                </div>
            </div>
            <div class="row pt-3 no-gutters">
                <div class="col-md-8">
                    
                </div>
                <div class="col-md-4 text-right">
                    <button type="submit" class="btn btn-info">KIRIM PESAN</button>
                </div>
            </div>
        <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>

