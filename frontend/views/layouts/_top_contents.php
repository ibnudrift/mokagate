<?php 
use yii\helpers\Url;
use yii\helpers\Html;
?>
<section class="tops_head">
	<div class="inner">
		<div class="row">
			<div class="col-md-6">
				<div class="taglines_hd d-none d-sm-block"><img src="<?php echo \Yii::getAlias('@web').'/asset/images/slog-headers_mobilkar.jpg' ?>" alt="" class="img img-fluid"></div>
				<div class="d-block d-sm-none taglines_hd mx-auto">
					<img src="<?php echo \Yii::getAlias('@web').'/asset/images/logo-mblkarawangs_hdr.jpg' ?>" alt="" class="img img-fluid d-block mx-auto">
					<div class="py-1"></div>
					<!-- <img src="<?php echo \Yii::getAlias('@web').'/asset/images/tagline-lgo-mobiles.png' ?>" alt="" class="img img-fluid"> -->
				</div>
			</div>
			<div class="col-md-6">
				<div class="text-right bloc-right-logout">
					<!-- <a href="#">Logout</a> -->
					<ul class="list-inline">
					<?php 
					 if (Yii::$app->user->isGuest) { ?>
				        <li><a href="<?php echo Url::toRoute(['/site/login']) ?>">Login</a></li>
				    <?php } else {
				        $menuItems = '<li>'
				            . Html::beginForm(['/site/logout'], 'post')
				            . Html::submitButton(
				                'Logout (' . Yii::$app->user->identity->username . ')',
				                ['class' => 'btn btn-link logout']
				            )
				            . Html::endForm()
				            . '</li>';
				        echo $menuItems;
				    }
					?>
					</ul>
				</div>
			</div>
		</div>

		<div class="clear clearfix"></div>
	</div>

</section>

<div class="d-block d-sm-none">
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
	  <a class="navbar-brand" href="#">MENU</a>
	  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
	    <span class="navbar-toggler-icon"></span>
	  </button>
	  <div class="collapse navbar-collapse" id="navbarNavDropdown">
	    <ul class="navbar-nav">
			<li class="nav-item">
				<a class="nav-link" href="<?php echo Url::toRoute(['home/index']) ?>">DASHBOARD</a>
			</li>
			<?php if (!Yii::$app->user->isGuest): ?>
			<li class="nav-item">
				<a class="nav-link" href="<?php echo Url::toRoute(['home/newvehicle']) ?>">KENDARAAN BARU</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="<?php echo Url::toRoute(['home/kendaraan']) ?>">UPDATE KENDARAAN</a>
			</li>
			<li class="nav-item">
					<a class="nav-link" href="<?php echo Url::toRoute(['home/antrianpesan']) ?>">ANTRIAN PESAN</a>
			</li>
			<li class="nav-item">
					<a class="nav-link" href="<?php echo Url::toRoute(['home/wabirthday']) ?>">PESAN ULANG TAHUN</a>
			</li>
			<li class="nav-item">
					<a class="nav-link" href="<?php echo Url::toRoute(['home/wareminder']) ?>">PESAN REMINDER</a>
			</li>
			<li class="nav-item">
					<a class="nav-link" href="<?php echo Url::toRoute(['home/wablast']) ?>">KIRIM MASAL</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="http://app.whatsez.id/v2/" target="_blank">KIRIM PESAN WA</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="<?php echo Url::toRoute(['users/index']) ?>">LIST USER</a>
			</li>
			<?php endif; ?>
	    </ul>
	  </div>
	</nav>
</div>