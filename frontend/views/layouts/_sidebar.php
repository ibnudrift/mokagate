<?php 
use yii\helpers\Url;

?>
<!-- Sidebar  -->
        <nav id="sidebar">
            <div class="sidebar-header">
                <h3><img src="<?php echo \Yii::getAlias('@web').'/asset/images/logo-mblkarawangs_hdr.jpg' ?>" alt="" class="img img-fluid"></h3>
            </div>

            <ul class="list-unstyled components">
                <p class="tops_listn">menu</p>
                <li>
                    <a href="<?php echo Url::toRoute(['home/index']) ?>">DASHBOARD</a>
                </li>
                <?php if (!Yii::$app->user->isGuest): ?>
                <li>
                    <a href="<?php echo Url::toRoute(['home/newvehicle']) ?>">KENDARAAN BARU</a>
                </li>
                <li>
                    <a href="<?php echo Url::toRoute(['home/kendaraan']) ?>">UPDATE KENDARAAN</a>
                </li>
                <li>
                    <a href="<?php echo Url::toRoute(['home/antrianpesan']) ?>">ANTRIAN PESAN</a>
                </li>
                <li>
                    <a href="<?php echo Url::toRoute(['home/wabirthday']) ?>">PESAN ULANG TAHUN</a>
                </li>
                <li>
                    <a href="<?php echo Url::toRoute(['home/wareminder']) ?>">PESAN REMINDER</a>
                </li>
                <li>
                    <a href="<?php echo Url::toRoute(['home/wablast']) ?>">KIRIM MASAL</a>
                </li>
                <!-- <li>
                    <a href="<?php echo Url::toRoute(['home/antrianpesan']) ?>">ANTRIAN PESAN</a>
                </li> -->
                <li>
                    <a href="http://app.whatsez.id/v2/" target="_blank">KE WHATSEZ</a>
                </li>
                <!-- <li>
                    <a href="#">BACKUP DATABASE</a>
                </li> -->
                <li>
                    <a href="<?php echo Url::toRoute(['users/index']) ?>">LIST USER</a>
                </li>
                <?php endif; ?>
            </ul>

            <div class="blc-copyrights">
                <p>Copyright &copy; <?php echo date("Y") ?> - Mobil Karawang <br>
                All rights reserved</p>
            </div>
        </nav>