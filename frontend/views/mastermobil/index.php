<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Master Mobil';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="master-mobil-index">
    <h1><?= Html::encode($this->title) ?></h1>
    <p>
        <?= Html::a('Create Master Mobil', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            // 'id',
            'tgl_beli',
            'no_polisi',
            'atas_nama_stnk',
            // 'alamat_stnk:ntext',
            'no_rangka',
            'no_mesin',
            'jenis_kendaraan',
            'warna',
            //'tahun_kendaraan',
            //'tanggal_berlaku_pajak',
            //'nama_penjual',
            //'hp_penjual',
            //'alamat_penjual:ntext',
            //'tanggal_lahir_penjual',
            //'total_perbaikan',
            //'harga_beli',
            //'status_terjual',
            //'dok_stnk',
            //'dok_bpkb',
            //'dok_faktur',
            //'dok_copyktp_stnk',
            //'dok_surat_buka_blokir',
            //'dok_pelepasan_hak',
            //'dok_kwitansi_blangko',
            //'dok_ktp_penjual',
            //'dok_foto_penjual',
            //'dok_nota_bon1',
            //'dok_nota_bon2',
            //'dok_nota_bon3',
            //'dok_nota_bon4',
            //'dok_nota_bon5',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
