<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\MasterMobil */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="master-mobil-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'tgl_beli')->textInput() ?>

    <?= $form->field($model, 'no_polisi')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'atas_nama_stnk')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'alamat_stnk')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'no_rangka')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'no_mesin')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'jenis_kendaraan')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'warna')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tahun_kendaraan')->textInput() ?>

    <?= $form->field($model, 'tanggal_berlaku_pajak')->textInput() ?>

    <?= $form->field($model, 'nama_penjual')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'hp_penjual')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'alamat_penjual')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'tanggal_lahir_penjual')->textInput() ?>

    <?= $form->field($model, 'total_perbaikan')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'harga_beli')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'status_terjual')->textInput() ?>

    <?= $form->field($model, 'dok_stnk')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'dok_bpkb')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'dok_faktur')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'dok_copyktp_stnk')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'dok_surat_buka_blokir')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'dok_pelepasan_hak')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'dok_kwitansi_blangko')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'dok_ktp_penjual')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'dok_foto_penjual')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'dok_nota_bon1')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'dok_nota_bon2')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'dok_nota_bon3')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'dok_nota_bon4')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'dok_nota_bon5')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
