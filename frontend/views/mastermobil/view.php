<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\MasterMobil */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Master Mobils', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="master-mobil-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'tgl_beli',
            'no_polisi',
            'atas_nama_stnk',
            'alamat_stnk:ntext',
            'no_rangka',
            'no_mesin',
            'jenis_kendaraan',
            'warna',
            'tahun_kendaraan',
            'tanggal_berlaku_pajak',
            'nama_penjual',
            'hp_penjual',
            'alamat_penjual:ntext',
            'tanggal_lahir_penjual',
            'total_perbaikan',
            'harga_beli',
            'status_terjual',
            'dok_stnk',
            'dok_bpkb',
            'dok_faktur',
            'dok_copyktp_stnk',
            'dok_surat_buka_blokir',
            'dok_pelepasan_hak',
            'dok_kwitansi_blangko',
            'dok_ktp_penjual',
            'dok_foto_penjual',
            'dok_nota_bon1',
            'dok_nota_bon2',
            'dok_nota_bon3',
            'dok_nota_bon4',
            'dok_nota_bon5',
        ],
    ]) ?>

</div>
