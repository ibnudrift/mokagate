<?php

namespace frontend\controllers;

use Yii;
use yii\base\InvalidArgumentException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use backend\models\MasterMobil;
use backend\models\MasterGallery;
use backend\models\MobilDokumen;
use backend\models\MasterPembelian;
use backend\models\MasterKonsumen;
use frontend\models\PesanForm;
use frontend\models\BlastForm;
use frontend\models\ReminderForm;
use yii\web\UploadedFile;
use yii\helpers\Url;

/**
 * Site controller
 */
class HomeController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        $this->layout = "@app/views/layouts/column1.php";

        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup','createantrian'],
                'rules' => [
                    [
                        'actions' => ['signup','createantrian'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */

    public function actionIndex()
    {
        // Yii::$app->user->isGuest;
        if (!Yii::$app->user->id) {
            // return $this->goHome();
            return $this->redirect(['site/login']);
        }

        return $this->render('index');
    }

    public function actionKendaraan()
    {
        $model = MasterMobil::find()->orderBy([
            'id' => SORT_DESC,
        ])->all();
        $jml_kendaraan = count($model);
        return $this->render('list_kendaraan', ['model' => $model, 'total' => $jml_kendaraan]);
    }

    public function actionNewvehicle()
    {
        $model = new MasterMobil();
        $modelGallery = new MasterGallery();

        if ($model->load(Yii::$app->request->post())) {

            /*$model->dok_stnk = UploadedFile::getInstance($model,'dok_stnk');
                $model->dok_bpkb = UploadedFile::getInstance($model,'dok_bpkb');
                $model->dok_faktur = UploadedFile::getInstance($model,'dok_faktur');
                $model->dok_copyktp_stnk = UploadedFile::getInstance($model,'dok_copyktp_stnk');
                $model->dok_surat_buka_blokir = UploadedFile::getInstance($model,'dok_surat_buka_blokir');
                $model->dok_pelepasan_hak = UploadedFile::getInstance($model,'dok_pelepasan_hak');
                $model->dok_kwitansi_blangko = UploadedFile::getInstance($model,'dok_kwitansi_blangko');
                $model->dok_ktp_penjual = UploadedFile::getInstance($model,'dok_ktp_penjual');
                $model->dok_foto_penjual = UploadedFile::getInstance($model,'dok_foto_penjual');
                $model->dok_nota_bon1 = UploadedFile::getInstance($model,'dok_nota_bon1');
                $model->dok_nota_bon2 = UploadedFile::getInstance($model,'dok_nota_bon2');
                $model->dok_nota_bon3 = UploadedFile::getInstance($model,'dok_nota_bon3');
                $model->dok_nota_bon4 = UploadedFile::getInstance($model,'dok_nota_bon4');
                $model->dok_nota_bon5 = UploadedFile::getInstance($model,'dok_nota_bon5');*/

            if ($model->validate()) {
                /*$name_dok_stnk = mt_rand(10,100).'_'.substr(md5(time()),0,5).'-'.$model->dok_stnk->baseName . '.' . $model->dok_stnk->extension;
                    $name_dok_bpkb = mt_rand(10,100).'_'.substr(md5(time()),0,5).'-'.$model->dok_bpkb->baseName . '.' . $model->dok_bpkb->extension;
                    $name_dok_faktur = mt_rand(10,100).'_'.substr(md5(time()),0,5).'-'.$model->dok_faktur->baseName . '.' . $model->dok_faktur->extension;
                    if ($model->dok_copyktp_stnk !== null) {
                        $name_dok_copyktp_stnk = mt_rand(10,100).'_'.substr(md5(time()),0,5).'-'.$model->dok_copyktp_stnk->baseName . '.' . $model->dok_copyktp_stnk->extension;
                    }
                    if ($model->dok_surat_buka_blokir !== null) {
                        $name_dok_surat_buka_blokir = mt_rand(10,100).'_'.substr(md5(time()),0,5).'-'.$model->dok_surat_buka_blokir->baseName . '.' . $model->dok_surat_buka_blokir->extension;
                    }
                    if ($model->dok_pelepasan_hak !== null) {
                        $name_dok_pelepasan_hak = mt_rand(10,100).'_'.substr(md5(time()),0,5).'-'.$model->dok_pelepasan_hak->baseName . '.' . $model->dok_pelepasan_hak->extension;
                    }
                    if ($model->dok_kwitansi_blangko !== null) {
                        $name_dok_kwitansi_blangko = mt_rand(10,100).'_'.substr(md5(time()),0,5).'-'.$model->dok_kwitansi_blangko->baseName . '.' . $model->dok_kwitansi_blangko->extension;
                    }
                    if ($model->dok_ktp_penjual !== null) {
                        $name_dok_ktp_penjual = mt_rand(10,100).'_'.substr(md5(time()),0,5).'-'.$model->dok_ktp_penjual->baseName . '.' . $model->dok_ktp_penjual->extension;
                    }
                    if ($model->dok_foto_penjual !== null) {
                        $name_dok_foto_penjual = mt_rand(10,100).'_'.substr(md5(time()),0,5).'-'.$model->dok_foto_penjual->baseName . '.' . $model->dok_foto_penjual->extension;
                    }
                    if ($model->dok_nota_bon1 !== null) {
                        $name_dok_nota_bon1 = mt_rand(10,100).'_'.substr(md5(time()),0,5).'-'.$model->dok_nota_bon1->baseName . '.' . $model->dok_nota_bon1->extension;
                    }

                    if ($model->dok_nota_bon2 !== null) {
                        $name_dok_nota_bon2 = mt_rand(10,100).'_'.substr(md5(time()),0,5).'-'.$model->dok_nota_bon2->baseName . '.' . $model->dok_nota_bon2->extension;
                    }
                    if ($model->dok_nota_bon3 !== null) {
                        $name_dok_nota_bon3 = mt_rand(10,100).'_'.substr(md5(time()),0,5).'-'.$model->dok_nota_bon3->baseName . '.' . $model->dok_nota_bon3->extension;
                    }
                    if ($model->dok_nota_bon4 !== null) {
                        $name_dok_nota_bon4 = mt_rand(10,100).'_'.substr(md5(time()),0,5).'-'.$model->dok_nota_bon4->baseName . '.' . $model->dok_nota_bon4->extension;
                    }
                    if ($model->dok_nota_bon5 !== null) {
                        $name_dok_nota_bon5 = mt_rand(10,100).'_'.substr(md5(time()),0,5).'-'.$model->dok_nota_bon5->baseName . '.' . $model->dok_nota_bon5->extension;
                    }

                    $model->dok_stnk->saveAs('uploads/mobil/' . $name_dok_stnk);
                    $model->dok_bpkb->saveAs('uploads/mobil/' . $name_dok_bpkb);
                    $model->dok_faktur->saveAs('uploads/mobil/' . $name_dok_faktur);

                    if ($model->dok_copyktp_stnk !== null) {
                        $model->dok_copyktp_stnk->saveAs('uploads/mobil/' . $name_dok_copyktp_stnk);
                    }
                    if ($model->dok_surat_buka_blokir !== null) {
                        $model->dok_surat_buka_blokir->saveAs('uploads/mobil/' . $name_dok_surat_buka_blokir);
                    }
                    if ($model->dok_pelepasan_hak !== null) {
                        $model->dok_pelepasan_hak->saveAs('uploads/mobil/' . $name_dok_pelepasan_hak);
                    }
                    if ($model->dok_kwitansi_blangko !== null) {
                        $model->dok_kwitansi_blangko->saveAs('uploads/mobil/' . $name_dok_kwitansi_blangko);
                    }
                    if ($model->dok_ktp_penjual !== null) {
                        $model->dok_ktp_penjual->saveAs('uploads/mobil/' . $name_dok_ktp_penjual);
                    }
                    if ($model->dok_foto_penjual !== null) {
                        $model->dok_foto_penjual->saveAs('uploads/mobil/' . $name_dok_foto_penjual);
                    }
                    if ($model->dok_nota_bon1 !== null) {
                        $model->dok_nota_bon1->saveAs('uploads/mobil/' . $name_dok_nota_bon1);
                    }

                    if ($model->dok_nota_bon2 !== null) {
                        $model->dok_nota_bon2->saveAs('uploads/mobil/' . $name_dok_nota_bon2);
                    }
                    if ($model->dok_nota_bon3 !== null) {
                        $model->dok_nota_bon3->saveAs('uploads/mobil/' . $name_dok_nota_bon3);
                    }
                    if ($model->dok_nota_bon4 !== null) {
                        $model->dok_nota_bon4->saveAs('uploads/mobil/' . $name_dok_nota_bon4);
                    }
                    if ($model->dok_nota_bon5 != null) {
                        $model->dok_nota_bon5->saveAs('uploads/mobil/' . $name_dok_nota_bon5);
                    }

                    $model->dok_stnk = $name_dok_stnk;
                    $model->dok_bpkb = $name_dok_bpkb;
                    $model->dok_faktur = $name_dok_faktur;

                    if ($model->dok_copyktp_stnk !== null) {
                        $model->dok_copyktp_stnk = $name_dok_copyktp_stnk;
                    }
                    if ($model->dok_surat_buka_blokir !== null) {
                        $model->dok_surat_buka_blokir = $name_dok_surat_buka_blokir;
                    }
                    if ($model->dok_pelepasan_hak !== null) {
                        $model->dok_pelepasan_hak = $name_dok_pelepasan_hak;
                    }
                    if ($model->dok_kwitansi_blangko !== null) {
                        $model->dok_kwitansi_blangko = $name_dok_kwitansi_blangko;
                    }
                    if ($model->dok_ktp_penjual !== null) {
                        $model->dok_ktp_penjual = $name_dok_ktp_penjual;
                    }
                    if ($model->dok_foto_penjual !== null) {
                        $model->dok_foto_penjual = $name_dok_foto_penjual;
                    }
                    if ($model->dok_nota_bon1 !== null) {
                        $model->dok_nota_bon1 = $name_dok_nota_bon1;
                    }

                    if ($model->dok_nota_bon2 !== null) {
                        $model->dok_nota_bon2 = $name_dok_nota_bon2;
                    }
                    if ($model->dok_nota_bon3 !== null) {
                        $model->dok_nota_bon3 = $name_dok_nota_bon3;
                    }
                    if ($model->dok_nota_bon4 !== null) {
                        $model->dok_nota_bon4 = $name_dok_nota_bon4;
                    }
                    if ($model->dok_nota_bon5 !== null) {
                        $model->dok_nota_bon5 = $name_dok_nota_bon5;
                    }
                */

                // print_r($model->getAttributes());
                $model->save(false);

                $model->imageFiles = UploadedFile::getInstances($model, 'imageFiles');
                $model->upload_gallery($model->id);

                $model->imageDokumen = UploadedFile::getInstances($model, 'imageDokumen');
                $model->upload_dokumen($model->id);

                // if ($model->upload_gallery($model->id)) {
                //     return true;
                // }

                // return $this->render('create_kendaraan_confirm', ['model' => $model]);
                Yii::$app->session->setFlash('success', "Data has been saved!");
                return $this->redirect(['home/kendaraan']);
            } else {
                $errors = $model->errors;
                return $this->render('create_kendaraan', ['model' => $model, 'modelGallery' => $modelGallery]);
            }
        }

        return $this->render('create_kendaraan', ['model' => $model, 'modelGallery' => $modelGallery]);
    }

    public function actionNewvehicle_view($id)
    {
        // VIEW KENDARAAN
        $idsn = intval($id);
        $model = MasterMobil::findOne($idsn);

        $modelGallery = array();
        $modelGallery = MasterGallery::find()
            ->where('mobil_id = :mobil_id', [':mobil_id' => $idsn])
            ->all();

        $modelDocum = array();
        $modelDocum = MobilDokumen::find()
            ->where('mobil_id = :mobil_id', [':mobil_id' => $idsn])
            ->all();

        $modelPembelian = array();
        $modelPembelian = MasterPembelian::find()
            ->where('id_mobil = :mobil_id', [':mobil_id' => $idsn])
            ->one();

        return $this->render('viewsn_kendaraan', ['model' => $model, 'modelGallery' => $modelGallery, 'modelDocum' => $modelDocum, 'modelPembelian'=>$modelPembelian]);
    }

    public function actionNewvehicle_edit_master($id)
    {
        // VIEW KENDARAAN
        $idsn = intval($id);
        $model = MasterMobil::findOne($idsn);

        $modelGallery = array();
        $modelGallery = MasterGallery::find()
            ->where('mobil_id = :mobil_id', [':mobil_id' => $idsn])
            ->all();

        $modelDokumen = array();
        $modelDokumen = MobilDokumen::find()
            ->where('mobil_id = :mobil_id', [':mobil_id' => $idsn])
            ->all();


        if ($model->load(Yii::$app->request->post())) {

            /* $model->dok_stnk = UploadedFile::getInstance($model,'dok_stnk');
                $model->dok_bpkb = UploadedFile::getInstance($model,'dok_bpkb');
                $model->dok_faktur = UploadedFile::getInstance($model,'dok_faktur');
                $model->dok_copyktp_stnk = UploadedFile::getInstance($model,'dok_copyktp_stnk');
                $model->dok_surat_buka_blokir = UploadedFile::getInstance($model,'dok_surat_buka_blokir');
                $model->dok_pelepasan_hak = UploadedFile::getInstance($model,'dok_pelepasan_hak');
                $model->dok_kwitansi_blangko = UploadedFile::getInstance($model,'dok_kwitansi_blangko');
                $model->dok_ktp_penjual = UploadedFile::getInstance($model,'dok_ktp_penjual');
                $model->dok_foto_penjual = UploadedFile::getInstance($model,'dok_foto_penjual');
                $model->dok_nota_bon1 = UploadedFile::getInstance($model,'dok_nota_bon1');
                $model->dok_nota_bon2 = UploadedFile::getInstance($model,'dok_nota_bon2');
                $model->dok_nota_bon3 = UploadedFile::getInstance($model,'dok_nota_bon3');
                $model->dok_nota_bon4 = UploadedFile::getInstance($model,'dok_nota_bon4');
                $model->dok_nota_bon5 = UploadedFile::getInstance($model,'dok_nota_bon5');
            */

            if ($model->validate()) {
                /*
                $name_dok_stnk = mt_rand(10,100).'_'.substr(md5(time()),0,5).'-'.$model->dok_stnk->baseName . '.' . $model->dok_stnk->extension;
                    $name_dok_bpkb = mt_rand(10,100).'_'.substr(md5(time()),0,5).'-'.$model->dok_bpkb->baseName . '.' . $model->dok_bpkb->extension;
                    $name_dok_faktur = mt_rand(10,100).'_'.substr(md5(time()),0,5).'-'.$model->dok_faktur->baseName . '.' . $model->dok_faktur->extension;
                    if ($model->dok_copyktp_stnk !== null) {
                        $name_dok_copyktp_stnk = mt_rand(10,100).'_'.substr(md5(time()),0,5).'-'.$model->dok_copyktp_stnk->baseName . '.' . $model->dok_copyktp_stnk->extension;
                    }
                    if ($model->dok_surat_buka_blokir !== null) {
                        $name_dok_surat_buka_blokir = mt_rand(10,100).'_'.substr(md5(time()),0,5).'-'.$model->dok_surat_buka_blokir->baseName . '.' . $model->dok_surat_buka_blokir->extension;
                    }
                    if ($model->dok_pelepasan_hak !== null) {
                        $name_dok_pelepasan_hak = mt_rand(10,100).'_'.substr(md5(time()),0,5).'-'.$model->dok_pelepasan_hak->baseName . '.' . $model->dok_pelepasan_hak->extension;
                    }
                    if ($model->dok_kwitansi_blangko !== null) {
                        $name_dok_kwitansi_blangko = mt_rand(10,100).'_'.substr(md5(time()),0,5).'-'.$model->dok_kwitansi_blangko->baseName . '.' . $model->dok_kwitansi_blangko->extension;
                    }
                    if ($model->dok_ktp_penjual !== null) {
                        $name_dok_ktp_penjual = mt_rand(10,100).'_'.substr(md5(time()),0,5).'-'.$model->dok_ktp_penjual->baseName . '.' . $model->dok_ktp_penjual->extension;
                    }
                    if ($model->dok_foto_penjual !== null) {
                        $name_dok_foto_penjual = mt_rand(10,100).'_'.substr(md5(time()),0,5).'-'.$model->dok_foto_penjual->baseName . '.' . $model->dok_foto_penjual->extension;
                    }
                    if ($model->dok_nota_bon1 !== null) {
                        $name_dok_nota_bon1 = mt_rand(10,100).'_'.substr(md5(time()),0,5).'-'.$model->dok_nota_bon1->baseName . '.' . $model->dok_nota_bon1->extension;
                    }

                    if ($model->dok_nota_bon2 !== null) {
                        $name_dok_nota_bon2 = mt_rand(10,100).'_'.substr(md5(time()),0,5).'-'.$model->dok_nota_bon2->baseName . '.' . $model->dok_nota_bon2->extension;
                    }
                    if ($model->dok_nota_bon3 !== null) {
                        $name_dok_nota_bon3 = mt_rand(10,100).'_'.substr(md5(time()),0,5).'-'.$model->dok_nota_bon3->baseName . '.' . $model->dok_nota_bon3->extension;
                    }
                    if ($model->dok_nota_bon4 !== null) {
                        $name_dok_nota_bon4 = mt_rand(10,100).'_'.substr(md5(time()),0,5).'-'.$model->dok_nota_bon4->baseName . '.' . $model->dok_nota_bon4->extension;
                    }
                    if ($model->dok_nota_bon5 !== null) {
                        $name_dok_nota_bon5 = mt_rand(10,100).'_'.substr(md5(time()),0,5).'-'.$model->dok_nota_bon5->baseName . '.' . $model->dok_nota_bon5->extension;
                    }

                    $model->dok_stnk->saveAs('uploads/mobil/' . $name_dok_stnk);
                    $model->dok_bpkb->saveAs('uploads/mobil/' . $name_dok_bpkb);
                    $model->dok_faktur->saveAs('uploads/mobil/' . $name_dok_faktur);

                    if ($model->dok_copyktp_stnk !== null) {
                        $model->dok_copyktp_stnk->saveAs('uploads/mobil/' . $name_dok_copyktp_stnk);
                    }
                    if ($model->dok_surat_buka_blokir !== null) {
                        $model->dok_surat_buka_blokir->saveAs('uploads/mobil/' . $name_dok_surat_buka_blokir);
                    }
                    if ($model->dok_pelepasan_hak !== null) {
                        $model->dok_pelepasan_hak->saveAs('uploads/mobil/' . $name_dok_pelepasan_hak);
                    }
                    if ($model->dok_kwitansi_blangko !== null) {
                        $model->dok_kwitansi_blangko->saveAs('uploads/mobil/' . $name_dok_kwitansi_blangko);
                    }
                    if ($model->dok_ktp_penjual !== null) {
                        $model->dok_ktp_penjual->saveAs('uploads/mobil/' . $name_dok_ktp_penjual);
                    }
                    if ($model->dok_foto_penjual !== null) {
                        $model->dok_foto_penjual->saveAs('uploads/mobil/' . $name_dok_foto_penjual);
                    }
                    if ($model->dok_nota_bon1 !== null) {
                        $model->dok_nota_bon1->saveAs('uploads/mobil/' . $name_dok_nota_bon1);
                    }

                    if ($model->dok_nota_bon2 !== null) {
                        $model->dok_nota_bon2->saveAs('uploads/mobil/' . $name_dok_nota_bon2);
                    }
                    if ($model->dok_nota_bon3 !== null) {
                        $model->dok_nota_bon3->saveAs('uploads/mobil/' . $name_dok_nota_bon3);
                    }
                    if ($model->dok_nota_bon4 !== null) {
                        $model->dok_nota_bon4->saveAs('uploads/mobil/' . $name_dok_nota_bon4);
                    }
                    if ($model->dok_nota_bon5 != null) {
                        $model->dok_nota_bon5->saveAs('uploads/mobil/' . $name_dok_nota_bon5);
                    }

                    $model->dok_stnk = $name_dok_stnk;
                    $model->dok_bpkb = $name_dok_bpkb;
                    $model->dok_faktur = $name_dok_faktur;

                    if ($model->dok_copyktp_stnk !== null) {
                        $model->dok_copyktp_stnk = $name_dok_copyktp_stnk;
                    }
                    if ($model->dok_surat_buka_blokir !== null) {
                        $model->dok_surat_buka_blokir = $name_dok_surat_buka_blokir;
                    }
                    if ($model->dok_pelepasan_hak !== null) {
                        $model->dok_pelepasan_hak = $name_dok_pelepasan_hak;
                    }
                    if ($model->dok_kwitansi_blangko !== null) {
                        $model->dok_kwitansi_blangko = $name_dok_kwitansi_blangko;
                    }
                    if ($model->dok_ktp_penjual !== null) {
                        $model->dok_ktp_penjual = $name_dok_ktp_penjual;
                    }
                    if ($model->dok_foto_penjual !== null) {
                        $model->dok_foto_penjual = $name_dok_foto_penjual;
                    }
                    if ($model->dok_nota_bon1 !== null) {
                        $model->dok_nota_bon1 = $name_dok_nota_bon1;
                    }

                    if ($model->dok_nota_bon2 !== null) {
                        $model->dok_nota_bon2 = $name_dok_nota_bon2;
                    }
                    if ($model->dok_nota_bon3 !== null) {
                        $model->dok_nota_bon3 = $name_dok_nota_bon3;
                    }
                    if ($model->dok_nota_bon4 !== null) {
                        $model->dok_nota_bon4 = $name_dok_nota_bon4;
                    }
                    if ($model->dok_nota_bon5 !== null) {
                        $model->dok_nota_bon5 = $name_dok_nota_bon5;
                    }
                */

                $model->save(false);

                // Update Image Tambahan gambar.
                MasterGallery::deleteAll('mobil_id = :id', [':id'=>$model->id]);
                if (count($_POST['MasterMobilFiles2']) > 0) {
                    foreach ($_POST['MasterMobilFiles2'] as $key => $value) {
                        $modelImage = new MasterGallery;
                        if ($value != '') {
                            $modelImage->mobil_id = $model->id;
                            $modelImage->images = $value;
                            $modelImage->save(false);
                        }
                        
                    }
                }

                if (count($model->imageFiles) > 0) {
                    $model->imageFiles = UploadedFile::getInstances($model, 'imageFiles');
                    $model->upload_gallery($model->id);
                }

                // Update image tambahan gambar
                MobilDokumen::deleteAll('mobil_id = :id', [':id'=>$model->id]);
                if (count($_POST['imageDokumen2']) > 0) {
                    foreach ($_POST['imageDokumen2'] as $key => $value) {
                        $modelImage2 = new MobilDokumen;
                        if ($value != '') {
                            $modelImage2->mobil_id = $model->id;
                            $modelImage2->images = $value;
                            $modelImage2->save(false);
                        }
                        
                    }
                }
                if (count($model->imageDokumen) > 0) {
                    $model->imageDokumen = UploadedFile::getInstances($model, 'imageDokumen');
                    $model->upload_dokumen($model->id);
                }

                // return $this->render('create_kendaraan_confirm', ['model' => $model]);
                Yii::$app->session->setFlash('success', "Data has been saved!");
                return $this->redirect(['home/kendaraan']);
            } else {
                $errors = $model->errors;
                return $this->render('update_kendaraan_master', ['model' => $model, 'modelGallery' => $modelGallery, 'modelDokumen' => $modelDokumen]);
            }
        }

        return $this->render('update_kendaraan_master', ['model' => $model, 'modelGallery' => $modelGallery, 'modelDokumen'=>$modelDokumen]);
    }

    public function actionNewvehicle_edit($id)
    {
        // UPDATE KENDARAAN
        $idsn = intval($id);
        $model = MasterMobil::findOne($idsn);

        $modelGallery = array();
        $modelGallery = MasterGallery::find()
            ->where('mobil_id = :mobil_id', [':mobil_id' => $idsn])
            ->all();

        $modelDokumen = array();
        $modelDokumen = MobilDokumen::find()
            ->where('mobil_id = :mobil_id', [':mobil_id' => $idsn])
            ->all();

        $modelPembelian = array();
        $modelPembelian = MasterPembelian::find()
            ->where('id_mobil = :mobil_id', [':mobil_id' => $idsn])
            ->one(); 
        // $modelPembelian->scenario = 'scenariocreates';

        if (empty($modelPembelian)) {
            $modelPembelian = new MasterPembelian;
            $modelPembelian->scenario = 'scenariocreates';
        }

        if ($model->load(Yii::$app->request->post()) && $modelPembelian->load(Yii::$app->request->post())) {


            $modelPembelian->dok_ktp_pembeli = UploadedFile::getInstance($modelPembelian, 'dok_ktp_pembeli');
            $modelPembelian->dok_foto_pembeli = UploadedFile::getInstance($modelPembelian, 'dok_foto_pembeli');

            if ($model->validate() && $modelPembelian->validate()) {

                // echo "<pre>";
                // print_r( $modelPembelian->getAttributes() );
                // exit;
                if ($modelPembelian->dok_ktp_pembeli !== null) {
                    $name_dok_ktp_pembeli = mt_rand(10, 100) . '_' . substr(md5(time()), 0, 5) . '-' . $modelPembelian->dok_ktp_pembeli->baseName . '.' . $modelPembelian->dok_ktp_pembeli->extension;
                }
                if ($modelPembelian->dok_foto_pembeli !== null) {
                    $name_dok_foto_pembeli = mt_rand(10, 100) . '_' . substr(md5(time()), 0, 5) . '-' . $modelPembelian->dok_foto_pembeli->baseName . '.' . $modelPembelian->dok_foto_pembeli->extension;
                }

                if ($modelPembelian->dok_ktp_pembeli !== null) {
                    $modelPembelian->dok_ktp_pembeli->saveAs('uploads/mobil/' . $name_dok_ktp_pembeli);
                    $modelPembelian->dok_ktp_pembeli = $name_dok_ktp_pembeli;
                }
                if ($modelPembelian->dok_foto_pembeli != null) {
                    $modelPembelian->dok_foto_pembeli->saveAs('uploads/mobil/' . $name_dok_foto_pembeli);
                    $modelPembelian->dok_foto_pembeli = $name_dok_foto_pembeli;
                }

                // save customer
                if ($modelPembelian->nama_pembeli != '') {
                    
                    $m_konsumen = new MasterKonsumen;
                    $m_konsumen->nama_pembeli = $modelPembelian->nama_pembeli;
                    $m_konsumen->alamat_pembeli = $modelPembelian->alamat_pembeli;
                    $m_konsumen->no_hp_pembeli = $modelPembelian->no_hp_pembeli;
                    $m_konsumen->tanggal_lahir = $modelPembelian->tanggal_lahir_pembeli;
                    $m_konsumen->status = 1;
                    $m_konsumen->save(false);

                    $curl = new \linslin\yii2\curl\Curl();
                    $response = $curl->setGetParams([
                        'access_token' => Yii::$app->params['accessToken'],
                    ])
                    ->setPostParams([
                        'nama' => $m_konsumen->nama_pembeli,
                        'panggilan' => $m_konsumen->nama_pembeli,
                        'sapaan' => $m_konsumen->nama_pembeli,
                        'no_wa' => $m_konsumen->no_hp_pembeli,
                        'tgl_lahir' => $m_konsumen->tanggal_lahir,
                        'kelompok' => 1,
                    ])
                    ->post(Yii::$app->params['urlServer'] . '/daftarnama/index');

                    $result = json_decode($response);
                    if ($result->status == '1') {
                        // Yii::$app->session->setFlash('success', "Contact Berhasil Di buat");
                        // return $this->redirect(['index']);
                    } else {
                        Yii::$app->session->setFlash('error', $result->status);
                        // return $this->redirect(Yii::$app->request->referrer ?: ['index']);
                    }
                }

                $modelPembelian->no_invoice = 'INV-KAR' . date('Ymd') . mt_rand(10, 1000);
                $modelPembelian->id_mobil =  $model->id;
                $modelPembelian->jenis_beli = $_POST['MasterPembelian']['jenis_beli'];
                $modelPembelian->perusahaan_kredit = $_POST['MasterPembelian']['perusahaan_kredit'];
                $modelPembelian->status = ($model->status_terjual == 1) ? 1 : 0;
                $modelPembelian->id_pembeli =  $m_konsumen->id;
                $model->save(false);
                $modelPembelian->save(false);

                Yii::$app->session->setFlash('success', "Data has been edited!");
                return $this->redirect(['home/kendaraan']);
            } else {
                $errors = $model->errors;
                return $this->render('updaten_kendaraan', ['model' => $model, 'modelGallery' => $modelGallery, 'modelPembelian' => $modelPembelian]);
            }
        }

        return $this->render('updaten_kendaraan', ['model' => $model, 'modelGallery' => $modelGallery, 'modelPembelian' => $modelPembelian, 'modelDokumen' => $modelDokumen]);
    }


    public function actionWabirthday()
    {
        $model = [];

        $curl = new \linslin\yii2\curl\Curl();
        $response = $curl->setGetParams([
            'access_token' => Yii::$app->params['accessToken'],
        ])
            ->get(Yii::$app->params['urlServer'] . '/pesan/ulangtahun');
        $result = json_decode($response);
        if ($result->status == '1') {
            // print_r($result);
            // exit;
        } else {
            // Yii::$app->session->setFlash('error', $result->status);
            // return $this->redirect(Yii::$app->request->referrer ?: ['index']);
        }

        $model = new PesanForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $curl = new \linslin\yii2\curl\Curl();
            if ($model->is_active == '0') {
                $model->pesan = '';
            }
            if (isset($result->data->pesan)) {
                $response = $curl->setGetParams([
                    'access_token' => Yii::$app->params['accessToken'],
                ])
                    ->setPostParams([
                        'pesan' => $model->pesan,
                    ])
                    ->put(Yii::$app->params['urlServer'] . '/pesan/ulangtahun/id/'.$result->data->id);

            }else{
                $response = $curl->setGetParams([
                    'access_token' => Yii::$app->params['accessToken'],
                ])
                    ->setPostParams([
                        'pesan' => $model->pesan,
                    ])
                    ->post(Yii::$app->params['urlServer'] . '/pesan/ulangtahun');
            }
            $result = json_decode($response);
            // print_r($result);
            // exit;
            if ($result->status == '1') {
                Yii::$app->session->setFlash('success', "Data has been edited!");
                return $this->refresh();
            } else {
                Yii::$app->session->setFlash('error', $result->status);
                // return $this->redirect(Yii::$app->request->referrer ?: ['index']);
            }
        }
        if (isset($result->data->pesan)) {
            $model->pesan = $result->data->pesan;
            if ($result->data->pesan != '') {
                $model->is_active = 1;
            } else {
                $model->is_active = 0;
            }
        }

        return $this->render('wa_birthday', [
            'result' => $result,
            'model' => $model,
        ]);
    }

    public function actionWahariraya()
    {
        $model = [];
        return $this->render('wa_hariraya');
    }

    public function actionWablast()
    {
        $model = [];

        $model = new BlastForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $curl = new \linslin\yii2\curl\Curl();
            $response = $curl->setGetParams([
                'access_token' => Yii::$app->params['accessToken'],
            ])
                ->setPostParams([
                    'pesan' => $model->pesan,
                    'page' => 1,
                    'limit' => 100000000000,
                    'agama' => '',
                    'jenis_kelamin' => '',
                    'type_list' => '',
                    'kelompok' => '',
                    'konteks' => '',
                ])
                ->post(Yii::$app->params['urlServer'] . '/masal/kirim');

            $result = json_decode($response);
            if ($result->status == '1') {

                // $curl = new \linslin\yii2\curl\Curl();
                // $response = $curl
                // ->setPostParams([
                //     'token' => Yii::$app->params['accessToken'],
                //     'limit' => 10000000000,
                //  ])
                // ->post(Yii::$app->params['urlWebChat'].'/api/sendmessage');
                // $result2 = json_decode($response);

                Yii::$app->session->setFlash('success', $result->data);
                return $this->refresh();
            } else {
                Yii::$app->session->setFlash('error', $result->status);
                // return $this->redirect(Yii::$app->request->referrer ?: ['index']);
            }
        }

        return $this->render('wa_blastpesan', [
            'model' => $model,
        ]);
    }

     public function actionAntrianpesan()
    {
        $model = [];
        $curl = new \linslin\yii2\curl\Curl();
        $response = $curl->setGetParams([
            'access_token' => Yii::$app->params['accessToken'],
        ])
            ->get(Yii::$app->params['urlServer'] . '/queue/index');
            // print_r($response);
            // exit;
        $result = json_decode($response);
        if ($result->status == '1') {
            // print_r($result);
            // exit;
        } else {
            Yii::$app->session->setFlash('error', $result->status);
            // return $this->redirect(Yii::$app->request->referrer ?: ['index']);
        }

        return $this->render('wa_antrianpesan', [
            'data' => $result->data,
        ]);
    }

    public function actionWareminder()
    {
        $curl = new \linslin\yii2\curl\Curl();
        $response = $curl->setGetParams([
            'access_token' => Yii::$app->params['accessToken'],
        ])
            ->get(Yii::$app->params['urlServer'] . '/pesan/index');
        $result = json_decode($response);
        if ($result->status == '1') {
            // print_r($result);
            // exit;
        } else {
            Yii::$app->session->setFlash('error', $result->status);
            // return $this->redirect(Yii::$app->request->referrer ?: ['index']);
        }

        $model = new ReminderForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $curl = new \linslin\yii2\curl\Curl();
            if ($model->id != '') {
                if ($model->is_active == '0') {
                    $response = $curl->setGetParams([
                        'access_token' => Yii::$app->params['accessToken'],
                    ])
                        ->delete(Yii::$app->params['urlServer'] . '/pesan/index/id/'.$model->id);
                }else{
                    $response = $curl->setGetParams([
                        'access_token' => Yii::$app->params['accessToken'],
                    ])
                        ->setPostParams([
                            'pesan' => $model->pesan,
                            'hari_ke' => $model->hari_ke,
                            'kelompok' => $model->kelompok,
                        ])
                        ->put(Yii::$app->params['urlServer'] . '/pesan/index/id/'.$model->id);
                }
            }else{
                $response = $curl->setGetParams([
                    'access_token' => Yii::$app->params['accessToken'],
                ])
                    ->setPostParams([
                        'pesan' => $model->pesan,
                        'hari_ke' => $model->hari_ke,
                        'kelompok' => $model->kelompok,
                    ])
                    ->post(Yii::$app->params['urlServer'] . '/pesan/index');
            }
            $result = json_decode($response);
            // print_r($result);
            // exit;
            if ($result->status == '1') {
                Yii::$app->session->setFlash('success', "Data has been edited!");
                return $this->refresh();
            } else {
                Yii::$app->session->setFlash('error', $result->status);
                // return $this->redirect(Yii::$app->request->referrer ?: ['index']);
            }
        }

        return $this->render('wa_reminder', [
            'result' => $result,
            'model' => $model,
        ]);
    }
    public function actionAntrian()
    {
        $model = MasterMobil::find()->orderBy([
            'id' => SORT_DESC,
        ])->all();
        $jml_kendaraan = count($model);
        return $this->render('list_kendaraan', ['model' => $model, 'total' => $jml_kendaraan]);
    }

    public function actionCreateantrian()
    {
        $curl = new \linslin\yii2\curl\Curl();
        $response = $curl->setGetParams([
                'access_token' => Yii::$app->params['accessToken'],
             ])
            ->get(Yii::$app->params['urlServer'].'/pengguna/count');

        $result = json_decode($response);
        // print_r($result);
        $curl = new \linslin\yii2\curl\Curl();
        // echo $user->token;
        $response = $curl
            ->setPostParams([
                'token' => Yii::$app->params['accessToken2'],
                ])
            ->post(Yii::$app->params['urlWebChat'].'/api/sendmessage');

        // print_r($response);
        // exit;
        $result = json_decode($response);
        return 1;
    }

    public function actionExeantrian()
    {
        $curl = new \linslin\yii2\curl\Curl();
        // echo $user->token;
        $response = $curl
            ->setPostParams([
                'access_token' => Yii::$app->params['accessToken'],
                'limit' => 1000000000000000,
                ])
            ->post(Yii::$app->params['urlWebChat'].'/api/sendmessage');

        // print_r($response);
        // exit;
        $result = json_decode($response);
        if ($result->status == '1') {
            Yii::$app->session->setFlash('success', $result->msg);
            return $this->redirect(['antrianpesan']);
        }else{
            Yii::$app->session->setFlash('error', $result->message);
            return $this->redirect(['antrianpesan']);
        }
    }

    public function actionDeleteall()
    {
        $curl = new \linslin\yii2\curl\Curl();
        
        $curl = new \linslin\yii2\curl\Curl();
        $response = $curl->setGetParams([
            'access_token' => Yii::$app->params['accessToken'],
        ])
        ->delete(Yii::$app->params['urlServer'].'/queue/deleteall');

        // $response = $curl
        //     ->setPostParams([
        //         'access_token' => Yii::$app->params['accessToken'],
        //         ])
        //         ->delete(Yii::$app->params['urlServer'].'/queue/deleteall');

        // print_r($response);
        // exit;

        $result = json_decode($response);
        if ($result->status == '1') {
            Yii::$app->session->setFlash('success', "Daftar Antrian Berhasil di Hapus");
            return $this->redirect(['antrianpesan']);
        }else{
            Yii::$app->session->setFlash('error', $result->message);
            return $this->redirect(['antrianpesan']);
        }
    }
}
