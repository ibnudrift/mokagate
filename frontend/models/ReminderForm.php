<?php

namespace frontend\models;

use Yii;
use yii\base\Model;


/**
 * This is the model class for table "pesan".
 *
 * @property int $id
 * @property string $user_id
 * @property int $hari_ke
 * @property string $pesan
 */
class ReminderForm extends Model
{
    public $is_active;
    public $pesan;
    public $hari_ke;
    public $id;
    public $kelompok;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['is_active', 'hari_ke'], 'required'],
            [['is_active', 'hari_ke', 'id', 'kelompok'], 'integer'],
            [['pesan'], 'string'],
            // [['user_id'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'is_active' => 'Is Active',
            'pesan' => 'Pesan Ulang tahun',
            'hari_ke' => 'Hari Ke',
        ];
    }
}
