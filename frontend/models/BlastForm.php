<?php

namespace frontend\models;

use Yii;
use yii\base\Model;


/**
 * This is the model class for table "pesan".
 *
 * @property int $id
 * @property string $user_id
 * @property int $hari_ke
 * @property string $pesan
 */
class BlastForm extends Model
{
    public $pesan;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pesan'], 'required'],
            [['pesan'], 'string'],
            // [['user_id'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pesan' => 'Pesan yang akan di blast',
        ];
    }
}
