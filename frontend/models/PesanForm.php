<?php

namespace frontend\models;

use Yii;
use yii\base\Model;


/**
 * This is the model class for table "pesan".
 *
 * @property int $id
 * @property string $user_id
 * @property int $hari_ke
 * @property string $pesan
 */
class PesanForm extends Model
{
    public $is_active;
    public $pesan;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['is_active'], 'required'],
            [['is_active'], 'integer'],
            [['pesan'], 'string'],
            // [['user_id'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'is_active' => 'Is Active',
            'pesan' => 'Pesan Ulang tahun',
        ];
    }
}
