<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Pendaftar */

$this->title = 'Update Pendaftar: ' . $model->nomer;
$this->params['breadcrumbs'][] = ['label' => 'Pendaftars', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->nomer, 'url' => ['view', 'id' => $model->nomer]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="pendaftar-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
