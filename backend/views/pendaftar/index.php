<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Pendaftars';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pendaftar-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Pendaftar', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'nomer',
            'nama',
            'jk',
            'agama',
            'nilai_ijazah',
            //'asal_sekolah',
            //'alamat_sekolah',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
