<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Pendaftar */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pendaftar-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nomer')->textInput() ?>

    <?= $form->field($model, 'nama')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'jk')->dropDownList([ 'L' => 'L', 'P' => 'P', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'agama')->dropDownList([ 'Islam' => 'Islam', 'Hindu' => 'Hindu', 'Budha' => 'Budha', 'Kristen' => 'Kristen', 'Konghucu' => 'Konghucu', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'nilai_ijazah')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'asal_sekolah')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'alamat_sekolah')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
