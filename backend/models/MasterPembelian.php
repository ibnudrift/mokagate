<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "master_pembelian".
 *
 * @property string $id
 * @property string $no_invoice
 * @property int $id_mobil
 * @property int $status_kendaraan
 * @property string $tanggal_penjualan
 * @property int $id_pembeli
 * @property string $nama_pembeli
 * @property string $alamat_pembeli
 * @property string $no_hp_pembeli
 * @property string $tanggal_lahir_pembeli
 * @property string $dok_ktp_pembeli
 * @property string $dok_foto_pembeli
 * @property int $harga_jual
 * @property string $jenis_beli
 * @property string $perusahaan_kredit
 * @property string $tanggal_lunas
 * @property int $nominal_angsuran
 * @property int $status
 */
class MasterPembelian extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'master_pembelian';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_mobil', 'id_pembeli', 'harga_jual', 'nominal_angsuran', 'status'], 'integer'],
            [['tanggal_penjualan', 'tanggal_lahir_pembeli', 'tanggal_lunas', 'status_kendaraan'], 'safe'],
            [['alamat_pembeli'], 'string'],
            [['no_invoice', 'nama_pembeli', 'no_hp_pembeli', 'dok_ktp_pembeli', 'dok_foto_pembeli', 'perusahaan_kredit'], 'string', 'max' => 225],
            [['jenis_beli'], 'string', 'max' => 10],
        ];
    }

    const SCENARIOCRIATE = 'scenariocreates';

    public function getCustomScenarios()
    {
      
      return [
          self::SCENARIOCRIATE      =>  ['tanggal_penjualan', 'tanggal_lahir_pembeli', 'tanggal_lunas', 'harga_jual','nominal_angsuran', 'nama_pembeli', 'alamat_pembeli', 'no_hp_pembeli', 'tanggal_lahir_pembeli'],
      ];
    }
    // get scenarios
    public function scenarios()
    {
        $scenarios = $this->getCustomScenarios();
        return $scenarios;
    }


    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'no_invoice' => 'No Invoice',
            'id_mobil' => 'Id Mobil',
            'status_kendaraan' => 'Status Kendaraan',
            'tanggal_penjualan' => 'Tanggal Penjualan',
            'id_pembeli' => 'Id Pembeli',
            'nama_pembeli' => 'Nama Pembeli',
            'alamat_pembeli' => 'Alamat Pembeli',
            'no_hp_pembeli' => 'No Hp Pembeli',
            'tanggal_lahir_pembeli' => 'Tanggal Lahir Pembeli',
            'dok_ktp_pembeli' => 'Dok Ktp Pembeli',
            'dok_foto_pembeli' => 'Dok Foto Pembeli',
            'harga_jual' => 'Harga Jual',
            'jenis_beli' => 'Jenis Beli',
            'perusahaan_kredit' => 'Perusahaan Kredit',
            'tanggal_lunas' => 'Tanggal Lunas',
            'nominal_angsuran' => 'Nominal Angsuran',
            'status' => 'Status',
        ];
    }
}
