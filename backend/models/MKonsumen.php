<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "master_konsumen".
 *
 * @property string $id
 * @property string $nama_pembeli
 * @property string $alamat_pembeli
 * @property string $no_hp_pembeli
 * @property string $tanggal_lahir
 * @property string $agama
 * @property string $email
 * @property int $status
 */
class MKonsumen extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'master_konsumen';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['alamat_pembeli'], 'string'],
            [['tanggal_lahir'], 'safe'],
            [['status'], 'integer'],
            [['nama_pembeli', 'no_hp_pembeli', 'agama', 'email'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama_pembeli' => 'Nama Pembeli',
            'alamat_pembeli' => 'Alamat Pembeli',
            'no_hp_pembeli' => 'No Hp Pembeli',
            'tanggal_lahir' => 'Tanggal Lahir',
            'agama' => 'Agama',
            'email' => 'Email',
            'status' => 'Status',
        ];
    }
}
