<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "mobil_gallery".
 *
 * @property string $id
 * @property int $id_str
 * @property int $mobil_id
 * @property string $images
 */
class MasterGallery extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'mobil_gallery';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_str', 'mobil_id'], 'integer'],
            [['images'], 'string', 'max' => 225],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_str' => 'Id Str',
            'mobil_id' => 'Mobil ID',
            'images' => 'Foto',
        ];
    }
}
