<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "user".
 *
 * @property int $id
 * @property string $username
 * @property string $auth_key
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property int $status
 * @property int $created_at
 * @property int $updated_at
 */
class User extends \yii\db\ActiveRecord
{
    public $password;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['username', 'auth_key', 'password_hash', 'email', 'created_at', 'updated_at'], 'required'],
            [['status', 'created_at', 'updated_at'], 'integer'],
            [['username', 'password_hash', 'password_reset_token', 'email'], 'string', 'max' => 255],
            [['auth_key'], 'string', 'max' => 32],
            [['username'], 'unique'],
            [['password', 'nama', 'telp', 'jabatan'], 'safe'],
            [['email'], 'unique'],
            [['password_reset_token'], 'unique'],
        ];
    }

    const SCENARIOCRIATE = 'scenariocreates';
    const SCENARIOUPDATES = 'scenariocupdates';
    public function getCustomScenarios()
    {
      
      return [
          self::SCENARIOCRIATE      =>  ['username', 'email', 'status', 'nama', 'telp', 'jabatan', 'password'],
          self::SCENARIOUPDATES      =>  ['username', 'email', 'status', 'nama', 'telp', 'jabatan', 'password'],
      ];
    }
    // get scenarios
    public function scenarios()
    {
        $scenarios = $this->getCustomScenarios();
        return $scenarios;
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'auth_key' => 'Auth Key',
            'password_hash' => 'Password Hash',
            'password_reset_token' => 'Password Reset Token',
            'email' => 'Email',
            'status' => 'Status',
            'telp' => 'Telepon',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
