<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\web\UploadedFile;

use backend\models\MasterGallery;
use backend\models\MobilDokumen;

/**
 * This is the model class for table "master_mobil".
 *
 * @property string $id
 * @property string $tgl_beli
 * @property string $no_polisi
 * @property string $atas_nama_stnk
 * @property string $alamat_stnk
 * @property string $no_rangka
 * @property string $no_mesin
 * @property string $jenis_kendaraan
 * @property string $warna
 * @property int $tahun_kendaraan
 * @property string $tanggal_berlaku_pajak
 * @property string $nama_penjual
 * @property string $hp_penjual
 * @property string $alamat_penjual
 * @property string $tanggal_lahir_penjual
 * @property string $total_perbaikan
 * @property string $harga_beli
 * @property int $status_terjual
 * @property string $dok_stnk
 * @property string $dok_bpkb
 * @property string $dok_faktur
 * @property string $dok_copyktp_stnk
 * @property string $dok_surat_buka_blokir
 * @property string $dok_pelepasan_hak
 * @property string $dok_kwitansi_blangko
 * @property string $dok_ktp_penjual
 * @property string $dok_foto_penjual
 * @property string $dok_nota_bon1
 * @property string $dok_nota_bon2
 * @property string $dok_nota_bon3
 * @property string $dok_nota_bon4
 * @property string $dok_nota_bon5
 */
class MasterMobil extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'master_mobil';
    }

    /**
     * {@inheritdoc}
     */
    // public $dok_stnk,
    //         $dok_bpkb,
    //         $dok_faktur,
    //         $dok_copyktp_stnk,
    //         $dok_surat_buka_blokir,
    //         $dok_pelepasan_hak,
    //         $dok_kwitansi_blangko,
    //         $dok_ktp_penjual,
    //         $dok_foto_penjual,
    //         $dok_nota_bon1,
    //         $dok_nota_bon2,
    //         $dok_nota_bon3,
    //         $dok_nota_bon4,
    //         $dok_nota_bon5;
    // public $tanggal_penjualan, $nama_pembeli, $alamat_pembeli, $no_hp_pembeli, $harga_jual, $tanggal_lahir_pembeli, $jenis_beli, $perusahaan_kredit, $tanggal_lunas, $nominal_angsuran, $dok_ktp_pembeli, $dok_foto_pembeli;
    public $imageFiles;
    public $imageDokumen;
    public $imageFiless2;
    public $imageDokumen2;

    public function rules()
    {
        return [
            [['tgl_beli', 'tanggal_berlaku_pajak', 'tanggal_lahir_penjual', 'dok_stnk', 'dok_bpkb', 'dok_faktur', 'dok_copyktp_stnk', 'dok_surat_buka_blokir', 'dok_pelepasan_hak', 'dok_kwitansi_blangko', 'dok_ktp_penjual', 'dok_foto_penjual', 'dok_nota_bon1', 'dok_nota_bon2', 'dok_nota_bon3', 'dok_nota_bon4', 'dok_nota_bon5', 'status_terjual', 'total_perbaikan'], 'safe'],

            [['alamat_stnk', 'alamat_penjual'], 'string'],
            ['no_polisi', 'unique'],
            [['tahun_kendaraan'], 'integer'],

            [['no_polisi', 'atas_nama_stnk', 'no_rangka', 'no_mesin', 'jenis_kendaraan', 'warna'], 'string', 'max' => 225],

            [['nama_penjual', 'hp_penjual', 'harga_beli'], 'string', 'max' => 100],

            [['dok_stnk', 'dok_bpkb', 'dok_faktur', 'dok_copyktp_stnk', 'dok_surat_buka_blokir', 'dok_pelepasan_hak', 'dok_kwitansi_blangko', 'dok_ktp_penjual', 'dok_foto_penjual', 'dok_nota_bon1'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, jpeg'],

            [['dok_nota_bon2', 'dok_nota_bon3', 'dok_nota_bon4', 'dok_nota_bon5'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, jpeg'],

             [['imageFiles'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, jpeg, gif', 'maxSize' => 1024 * 1024 * 50, 'maxFiles' => 30],
             [['imageDokumen'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, jpeg, gif', 'maxSize' => 1024 * 1024 * 50, 'maxFiles' => 30],
        ];
    }

    public function upload_gallery($id_mobil)
    {
        if ($this->validate()) {
            foreach ($this->imageFiles as $file) {
                $sm_nameImage1 = mt_rand(5, 100).'_'. $file->baseName . '.' . $file->extension;

                $file->saveAs('uploads/gallery/' . $sm_nameImage1);

                $m_gallerys = new MasterGallery;
                $m_gallerys->mobil_id = $id_mobil;
                $m_gallerys->images = $sm_nameImage1;
                $m_gallerys->save(false);
            }
            return true;
        } else {
            return false;
        }
    }

    public function upload_dokumen($id_mobil)
    {
        if (count($this->imageDokumen) > 0) {
            foreach ($this->imageDokumen as $file2) {
                $sm_nameImage = mt_rand(5, 100).'_'. $file2->baseName . '.' . $file2->extension;

                $file2->saveAs('uploads/dokumen/' . $sm_nameImage);

                $m_dokumens = new MobilDokumen;
                $m_dokumens->id_str = 0;
                $m_dokumens->mobil_id = $id_mobil;
                $m_dokumens->images = $sm_nameImage;
                $m_dokumens->save(false);
            }
            return true;
        } else {
            return false;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tgl_beli' => 'Tanggal Beli',
            'no_polisi' => 'No Polisi',
            'atas_nama_stnk' => 'Atas Nama Stnk',
            'alamat_stnk' => 'Alamat Stnk',
            'no_rangka' => 'No Rangka',
            'no_mesin' => 'No Mesin',
            'jenis_kendaraan' => 'Jenis Kendaraan',
            'warna' => 'Warna',
            'tahun_kendaraan' => 'Tahun Kendaraan',
            'tanggal_berlaku_pajak' => 'Tanggal Berlaku Pajak',
            'nama_penjual' => 'Nama Penjual',
            'hp_penjual' => 'Hp Penjual',
            'alamat_penjual' => 'Alamat Penjual',
            'tanggal_lahir_penjual' => 'Tanggal Lahir Penjual',
            'total_perbaikan' => 'Total Perbaikan',
            'harga_beli' => 'Harga Beli',
            'status_terjual' => 'Status Terjual',
            'dok_stnk' => 'STNK',
            'dok_bpkb' => 'BPKB',
            'dok_faktur' => 'FAKTUR',
            'dok_copyktp_stnk' => 'COPY KTP ATAS NAMA STNK',
            'dok_surat_buka_blokir' => 'SURAT BUKA BLOKIR',
            'dok_pelepasan_hak' => 'SURAT PELEPASAN HAK',
            'dok_kwitansi_blangko' => 'KUITANSI BLANGKO',
            'dok_ktp_penjual' => 'KTP PENJUAL',
            'dok_foto_penjual' => 'FOTO PENJUAL',
            'dok_nota_bon1' => 'ANEKA NOTA / BON 1',
            'dok_nota_bon2' => 'ANEKA NOTA / BON 2',
            'dok_nota_bon3' => 'ANEKA NOTA / BON 3',
            'dok_nota_bon4' => 'ANEKA NOTA / BON 4',
            'dok_nota_bon5' => 'ANEKA NOTA / BON 5',
        ];
    }

    public function getGallerys()
    {
        return $this->hasMany(MasterGallery::className(), ['mobil_id' => 'id']);
    }

}
