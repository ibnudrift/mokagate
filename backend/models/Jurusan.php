<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "jurusan".
 *
 * @property int $kode_jurusan
 * @property string $nama_jurusan
 */
class Jurusan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'jurusan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kode_jurusan'], 'required'],
            [['kode_jurusan'], 'integer'],
            [['nama_jurusan'], 'string', 'max' => 32],
            [['kode_jurusan'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'kode_jurusan' => 'Kode Jurusan',
            'nama_jurusan' => 'Nama Jurusan',
        ];
    }
}
