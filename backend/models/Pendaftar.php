<?php

namespace backend\models;

class Pendaftar extends \yii\db\ActiveRecord
{

    // membuat nama tabel
    public static function tableName()
    {
        return 'pendaftar';
    }
    
    
    // membuat aturan
    public function rules()
    {
        return [
            // #1 Data harus terisi semua
            [['nomer', 'nama', 'jk', 'agama', 'nilai_ijazah', 'asal_sekolah', 'alamat_sekolah'], 'required'],
            
            // #2 nomer pedaftaran harus berupa angka
            [['nomer'], 'integer'],
            
            // #3 Selain nomer, datanya berupa string
            [['nama', 'jk', 'agama', 'nilai_ijazah', 'asal_sekolah', 'alamat_sekolah'], 'string']
        ];
    }
    
    
    // membuat label atribut
    public function attributeLabels()
    {
        return [
            'nomer' => 'Nomer Pendaftaran',
            'nama' => 'Nama',
            'jk' => 'Jenis Kelamin',
            'nilai_ijazah' => 'Nilai Ijazah',
            'asal_sekolah' => 'Sekolah Asal',
            'alamat_sekolah' => 'Alamat Sekolah'
        ];
    }

}